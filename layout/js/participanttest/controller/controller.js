var controller = Backbone.View.extend({
    
    tagName: 'div',
    id: 'wrapper',
    
    events:{
        'click .goToLastPage': 'callLastLoadedPage'
    },

    initialize: function () {
        _.bindAll(this, 'render');
        this.render();
    },
        
    render: function () {
        
        if(window.innerWidth <= 900){
            this.$el.html(
                '<!-- Page Content -->' +
                '<div id="test-wrapper"></div>' +
                '<!-- /#wrapper -->'
                );
        } else {
            this.$el.html(
                '<!-- Navigation -->' +
                '<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">' +
                    '<div class="  img-responsive logo-exide"></div>' +
                    '<div class=" img-responsive logo-tms">TRAINING MANAGEMENT SYSTEM</div>' +
                    /*'<div class=" img-responsive logo-vidya"></div>' +*/
                    '<div class="navbar-header">' +
                        '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">' +
                            '<span class="sr-only">Toggle navigation</span>' +
                            '<span class="icon-bar"></span>' +
                            '<span class="icon-bar"></span>' +
                            '<span class="icon-bar"></span>' +
                        '</button>' +
                    '</div>' +
                    '<!-- /.navbar-header -->' +
                    '<ul class="nav navbar-top-links navbar-right">' +
                        '<li class="dropdown">' +
                            '<a class="dropdown-toggle" data-toggle="dropdown" href="#">' +
                                '<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>' +
                            '</a>' +
                        '<ul class="dropdown-menu dropdown-user">' +
                                '<li>' +
                                    '<span id="module_name"></span>'+
                                '</li>' +
                                '<li id="employee_also" style="display: none"><a href="#"><i class="fa fa-user fa-fw"></i> Employee </a>' +
                                '</li>' +
                                '<li class="divider"></li>' +
                                '<li><a href="logout">' +
                                    '<i class="fa fa-sign-out fa-fw"></i>' +
                                        'Logout</a>' +
                                '</li>' +
                            '</ul>' +
                            '<!-- /.dropdown-user -->' +
                        '</li>' +
                    '</ul>' +
                '</nav>' +
            '<!-- Page Content -->' +
            '<div id="test-wrapper"></div>' +
            '<!-- /#wrapper -->'
            );
        }
        this.loadModuleName();
    },
        callLastLoadedPage: function(){
            window.history.back();
        },
    
        loadModuleName: function (e) {
            
        $("#pageloading").show();
            Backbone.ajax({
            dataType: "json",
            url: "getvariable?key=moduleName",
            data: "",
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function(data){
                $(".logo-tms").text(data);
            },
                complete: function() {
                    $("#pageloading").hide();
                }
        });
    },
});
(function () {
    
    var disableAnswerFlag, count, testTime;
    window.test_run = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    test_run.Views.testrun = Backbone.View.extend({
        
        events: {
            'click #next_section': 'nextSectionClick',
            'click .anschecked': 'triggerLeftTime',
            'click .mokanschecked': 'triggerMokLeftTime',
            'click .startTopicTest': 'testBeginHere',
            'click .startMockTest': 'mocTestBeginHere',
            'click #submit_test': 'finalTestSubmission',
            'click #back_section': 'preveiousSectionClick',
            'click #mok_submit': 'mockTestSubmit',
            'click .pre-que': 'openQuestionPanel',
            'click #preview_test': 'previewMockTestQuestion',
            'click #reviewAns': 'mocTestReviewAnswer'
        },
        
        initialize: function () {
            _.bindAll(this, 'render');
            count = 0;
            correctAns = 0;
            ansArray = [];
            queArray = [];
            disableAnswerFlag = false;
        },
        
        render: function () {
            var ranNum = Math.floor(Math.random() * 2001);
            
            this.$el.html(
                '<div class="row" id="test_page">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-heading">' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<span id="par-name" style="font-weight: bold;"> </span>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-4">' +
                                        'SECTION <span id="section"> A </span>' +
                                    '</div>' +
                                    '<div class="col-xs-4">' +
                                        'QUESTIONS : <span id="quesNo"> </span>' +
                                    '</div>' +
                                    /*'<div class="col-xs-4">' +
                                        'Time Remaining : <span id="timer"></span>' +
                                    '</div>' +*/
                                '</div>' +
                            '</div>' +
                            '<div class="panel-body" style="max-height: 425px; overflow-y: scroll; padding: 25px;">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="panel-group" id="test_questions"></div>' +
                                    '</div>' +
                                '</div>' +
                                '<!-- /.row (nested) -->' +
                            '</div>' +
                            '<!-- /.panel-body -->' +
                            '<div class="panel-footer">' +
                                '<div class="row">' +
                                    '<div class="col-xs-3 pull-right">' +
                                        '<button id="back_section" type="reset" class="btn btn-default">PREVIOUS</button>' +
                                        '<button id="next_section" type="submit" class="btn btn-default pull-right" style="margin-right: 30%;">NEXT</button>' +
                                        '<button id="submit_test" type="submit" class="btn btn-default pull-right" style="margin-right: 30%;">Submit</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<div class="modal fade" id="instructions_page" tabindex="-1" role="dialog" aria-labelledby="instructions_page" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                    '<div class="modal-dialog" style="width: 70%;">' +
                        '<div class="modal-content">' +
                            '<div class="modal-header">' +
                                '<h4>Instructions <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h4>'+
                            '</div>' +
                            '<div class="modal-body">' +
                                '<div class="panel-body" style="text-align: justify; padding:0 100px">' +
                                    '<div class="row">' +
                                        '<img src="instructiondisplay/'+ranNum+'" class="inst-jpg" alt="No Instruction">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                                '<button class="startTopicTest btn btn-default"> Test Start </button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );
            
            $("#instructions_page").modal({ keyboard: false });
            $("#instructions_page").modal('show');
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "topictestquestion",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                        questionElement = Backbone.$("#test_questions").empty();
                        testview.generateQuestions(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        mockTestBegin: function() {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "ismockcomp",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                	 $("#pageloading").hide();
                    if(data == 1) {
                        swal({
                            title: "Maximum attempts completed. ",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            var modId = $("#module_name").attr('value');
                            participantroutes.navigate('modules/'+modId, {trigger: true});
                        });
                    } else if(data == 2){
                        testview.mockAfterAssesmentComplete(data);
                    } else {
                        swal({
                            title: "Some problem while taking test. ",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            var modId = $("#module_name").attr('value');
                            participantroutes.navigate('modules/'+modId, {trigger: true});
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                    $("#pageloading").hide();
                },
                complete: function() {
                   
                }
            });
        },
        mockAfterAfterAssesmentComplete:function(){
        	 var ranNum = Math.floor(Math.random() * 2001);

            this.$el.html(
                '<div class="row" id="test_page">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<span id="par-name" style="font-weight: bold;"> </span>' +
                                    '</div>' +
                                '</div>' +
                            '<div class="panel-heading">' +
                                '<div class="row">' +
                                    '<div class="col-xs-4">' +
                                        'SECTION <span id="section"> A </span>' +
                                    '</div>' +
                                    '<div class="col-xs-4">' +
                                        'QUESTIONS : <span id="quesNo"></span>' +
                                    '</div>' +
                                    '<div class="col-xs-4">' +
                                        'Time Remaining : <span id="timer"></span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="panel-body" style="max-height: 425px; overflow-y: scroll; padding: 25px;">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="panel-group" id="test_questions"></div>' +
                                    '</div>' +
                                '</div>' +
                                '<!-- /.row (nested) -->' +
                            '</div>' +
                            '<!-- /.panel-body -->' +
                            '<div class="panel-footer">' +
                                '<div class="row">' +
                                        /*'<button id="mok_submit" type="submit" class="btn btn-danger pull-right" style="margin-right: 3%;">SUBMIT & EXIT</button>' +*/
                                        '<button id="preview_test" type="submit" class="btn btn-default pull-right" style="margin-right: 3%;">DONE</button>' +
                                    '<div class="col-xs-3 pull-right">' +
                                        '<button id="back_section" type="reset" class="btn btn-default">PREVIOUS</button>' +
                                        '<button id="next_section" type="submit" class="btn btn-default pull-right" style="margin-right: 30%;">NEXT</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<div class="modal fade" id="preview-mock" tabindex="-1" role="dialog" aria-labelledby="preview-mock" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                    '<div class="modal-dialog" style="width: 80%;">' +
                        '<div class="modal-content">' +
                            '<div class="modal-header">' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<div> Maximum Marks : <span id="max-que"> </span></div>' +
                                        '<div> Time Duration : <span id="test-durat"> </span></div>' +
                                     '</div>' +
                                    '<div class="col-xs-6">' +
                                        '<div class="pull-right"> Pass Marks : <span id="pass-marks"> </span></div><br>' +
                                        '<div class="pull-right"> Time Left : <span id="timer-module"> </span></div>' +
                                     '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="modal-body">' +
                                '<div class="panel-body" style="text-align: justify; padding:0 100px">' +
                                    '<div class="row" style="margin-bottom:10%;">' +
                                        '<div class="col-xs-6">' +
                                            '<div> Total No of Questions : <span id="tot-que"> </span></div>' +
                                            '<button type="button" class="btn btn-circle btn-lg btn-danger" style="margin:2px;"> </button> UN-ANSWERED' +
                                         '</div>' +
                                        '<div class="col-xs-6">' +
                                            '<div> No of Questions attemted : <span id="ans-que"> </span></div>' +
                                            '<button type="button" class="btn btn-circle btn-lg btn-success" style="margin:2px;"> </button> ANSWERED' +
                                         '</div>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<div class="col-xs-12" id="preview-quession">' +
                                         '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                                '<button id="mok_submit" type="submit" class="btn btn-success"> <i class="fa fa-external-link"></i> Submit answer paper </button>' +
                                '<button class="btn btn-success pull-left" align="center" data-dismiss="modal"> <i class="fa fa-sign-out fa-fw"></i> Go Back to question paper </button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="modal fade" id="instructions_page" tabindex="-1" role="dialog" aria-labelledby="instructions_page" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                    '<div class="modal-dialog" style="width: 70%;">' +
                        '<div class="modal-content">' +
                            '<div class="modal-header">' +
                                '<h4>Instructions <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h4>'+
                            '</div>' +
                            '<div class="modal-body">' +
                                '<div class="panel-body" style="text-align: justify; padding:0 100px">' +
                                    '<div id="int-pre" class="row">' +
                                        '<img src="instructiondisplay/'+ranNum+'" class="inst-jpg" alt="No Instruction">' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="modal-footer">' +
                                '<button class="startMockTest btn btn-default"> Test Start </button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>'
                );
            
            $("#instructions_page").modal({ keyboard: false });
            $("#instructions_page").modal('show');

        },
        mockAfterAssesmentComplete: function() {
           
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "mok-testquestion",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                    	testview.mockAfterAfterAssesmentComplete();
                    	
                    	
                    	
                        questionElement = Backbone.$("#test_questions").empty();
                        testview.mokTestGenerateQuestions(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        
        mokTestGenerateQuestions:function(data){
            for(var i=0; i< data.length; i++){
                var que = data[i].qid;
                var ansar = data[i].ans;
                queArray.push({questionId:que, answer:ansar});
                ansArray.push({questionId:que, answer:""});
                questionElement.append(
                '<div class="row q'+i+' quesClasses" id=quesid'+que+'>' +
                    '<div class="que-header"><b>'+(i+1)+'. </b><b>'+data[i].question+'</b></div>' +
                    '<div class="col-xs-6">' +
                        '<div class="radio">' +
                            '<label id="labOp1'+i+'">' +
                                '<input type="radio" class="mokanschecked" name="options'+i+'" value="opt1"><span class="valCls">'+data[i].opt1+'</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label id="labOp3'+i+'">' +
                                '<input type="radio" class="mokanschecked" name="options'+i+'" value="opt3"><span class="valCls">'+data[i].opt3+'</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label id="labOp5'+i+'">' +
                                '<input type="radio" class="mokanschecked" name="options'+i+'" value="opt5"><span class="valCls">'+data[i].opt5+'</span>' +
                            '</label>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-xs-6">' +
                        '<div class="radio">' +
                            '<label id="labOp2'+i+'">' +
                                '<input type="radio" class="mokanschecked" name="options'+i+'" value="opt2"><span class="valCls">'+data[i].opt2+'</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label id="labOp4'+i+'">' +
                                '<input type="radio" class="mokanschecked" name="options'+i+'" value="opt4"><span class="valCls">'+data[i].opt4+'</span>' +
                            '</label>' +
                        '</div>' +
                    '</div>' +
                '</div>'
                );
                if(data[i].opt5 == null){
                    $("#labOp5"+i+"").hide();
                } else if(data[i].opt5 == 0){
                    $("#labOp5"+i+"").hide();
                }
            }
            
            if(multipleQueFlag){
                var nqs = queArray.length;
                $("#quesNo").text(""+nqs+"/"+nqs+"");
                singleQueFlag = false;
                $(".quesClasses").show();
                
                
                $("#submit_test").show();
                $("#next_section").hide();
                $("#back_section").hide();
            } else if(singleQueFlag){
                var nqs = queArray.length;
                multipleQueFlag = false;
                $(".q0").show();
                $("#quesNo").text("1/"+nqs+"");
                
                $("#next_section").show();
                $("#back_section").hide();
                $("#submit_test").hide();
            }
            
        },
        
        
        testBeginHere: function() {
            $("#test_page").show();
            $("#instructions_page").modal('hide');
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getvariable?key=participantName",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data){
                    $("#par-name").text(data);
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
            /*
            $("#pageloading").show();
            Backbone.ajax({
                url: "starttest",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        swal({
                            title: "Please contanct IT admin",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            participantview.loadTopicTestStart();
                        });
                    } else if(data == "error"){
                        swal({
                            title: "Please contanct IT admin",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            participantview.loadTopicTestStart();
                        });
                    } else {
                        testview.timeRemaining(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
    
        
        mocTestBeginHere: function() {
            $("#test_page").show();
            $("#instructions_page").modal('hide');
            
            $("#pageloading").show();
            Backbone.ajax({
                url: "mok-teststart",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        swal({
                            title: "Please contanct IT admin",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            participantview.loadMockTestStart();
                        });
                    } else if(data == "error"){
                        swal({
                            title: "Please contanct IT admin",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            participantview.loadMockTestStart();
                        });
                    } else {
                        var timeD;
                        if(typeof(data) == "object"){
                            $("#max-que").text(data.maxScore);
                            $("#pass-marks").text(data.minScore);
                            timeD = data.testDuration;
                        } else {
                            $("#max-que").text(JSON.parse(data).maxScore);
                            $("#pass-marks").text(JSON.parse(data).minScore);
                            timeD = JSON.parse(data).testDuration;
                        }
                        timeD = timeD * 60;
                        display = $('#timer');
                        testview.timeRemaining(timeD, display);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getvariable?key=participantName",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function(data){
                    $("#par-name").text(data);
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        generateQuestions:function(data){
            for(var i=0; i< data.length; i++){
                var que = data[i].qid;
                var ansar = data[i].ans;
                queArray.push({questionId:que, answer:ansar});
                ansArray.push({questionId:que, answer:""});
                questionElement.append(
                '<div class="row q'+i+' quesClasses" id=quesid'+data[i].qid+'>' +
                    '<div class="que-header"><b>'+(i+1)+'. </b><b>'+data[i].question+'?</b></div>' +
                    '<div class="col-xs-6">' +
                        '<div class="radio">' +
                            '<label id="labOp1'+i+'">' +
                                '<input type="radio" class="anschecked" name="options'+i+'" value="opt1"><span class="valCls">'+data[i].opt1+'</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label id="labOp3'+i+'">' +
                                '<input type="radio" class="anschecked" name="options'+i+'" value="opt3"><span class="valCls">'+data[i].opt3+'</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label id="labOp5'+i+'">' +
                                '<input type="radio" class="anschecked" name="options'+i+'" value="opt5"><span class="valCls">'+data[i].opt5+'</span>' +
                            '</label>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-xs-6">' +
                        '<div class="radio">' +
                            '<label id="labOp2'+i+'">' +
                                '<input type="radio" class="anschecked" name="options'+i+'" value="opt2"><span class="valCls">'+data[i].opt2+'</span>' +
                            '</label>' +
                        '</div>' +
                        '<div class="radio">' +
                            '<label id="labOp4'+i+'">' +
                                '<input type="radio" class="anschecked" name="options'+i+'" value="opt4"><span class="valCls">'+data[i].opt4+'</span>' +
                            '</label>' +
                        '</div>' +
                    '</div>' +
                '</div>'
                );
                if(data[i].opt5 == null){
                    $("#labOp5"+i+"").hide();
                } else if(data[i].opt5 == 0){
                    $("#labOp5"+i+"").hide();
                }
            }
            
            if(multipleQueFlag){
                var nqs = queArray.length;
                $("#quesNo").text(""+nqs+"/"+nqs+"");
                singleQueFlag = false;
                $(".quesClasses").show();
                
                $("#submit_test").show();
                $("#next_section").hide();
                $("#back_section").hide();
            } else if(singleQueFlag){
                var nqs = queArray.length;
                multipleQueFlag = false;
                $(".q0").show();
                $("#quesNo").text("1/"+nqs+"");
                
                $("#next_section").show();
                $("#back_section").hide();
                $("#submit_test").hide();
            }
            
        },
        
        
        triggerMokLeftTime: function(e) {
            var que=e.currentTarget.parentElement.parentElement.parentElement.parentElement.id.slice(6);
            var ansid = e.currentTarget.value;
            for( var i = 0; i<ansArray.length; i++){
                if(ansArray[i].questionId == que){
                    ansArray[i].answer = ansid;
                }
            }
            
            $("#pageloading").show();
            Backbone.ajax({
                url: "mok-remainingtime",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 1) {
                        
                    } else if(data == 2) {
                        $('.mokanschecked').prop('disabled', true);
                        sweetAlert("Please submit your time is over");
                        disableAnswerFlag = true;
                        $("#next_section").hide();
                        clearInterval(testTime);
                        $('#timer').text("00 : 00 : 00" );
                        $('#timer-module').text("00 : 00 : 00" );
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        triggerLeftTime: function(e) {
            var que=e.currentTarget.parentElement.parentElement.parentElement.parentElement.id.slice(6);
            var ansid = e.currentTarget.value;
            for( var i = 0; i<ansArray.length; i++){
                if(ansArray[i].questionId == que){
                    ansArray[i].answer = ansid;
                }
            }
            
            /*$("#pageloading").show();
            Backbone.ajax({
                url: "remainingtime",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 1) {
                        
                    } else if(data == 2) {
                        $('.anschecked').prop('disabled', true);
                        sweetAlert("Please submit your time is over");
                        disableAnswerFlag = true;
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });*/
        },
        
        timeRemaining: function(duration, display) {
            
            $('#test-durat').text( duration/60 + " mins" );
            var timer = duration, hours, minutes, seconds;
            testTime = setInterval(function () {
                hours = parseInt(timer / 3600, 10);
                minutes = parseInt((timer % 3600) / 60, 10);
                seconds = parseInt(timer % 60, 10);

                hours = hours < 10 ? "0" + hours : hours;
                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.text(hours + " : " + minutes + " : " + seconds);
                $('#timer-module').text(hours + " : " + minutes + " : " + seconds);
                
                if (--timer < 0) {
                    clearInterval(testTime);
                    $('.anschecked').prop('disabled', true);
                    $('.mokanschecked').prop('disabled', true);
                    sweetAlert("Please submit your time is over");
                    disableAnswerFlag = true;
                    $("#next_section").hide();
                }
            }, 1000);
            /*var vald = parseInt(data)
            var num = vald/60;
            var hours = parseInt(num);
            var nmin = vald%60;
            var mins;
            if(nmin == 0){
                mins = 59;
                hours = hours - 1;
            } else {
                mins = nmin - 1;
            }
            var secs = 60;
            $('#test-durat').text( vald + " mins" );
            testTime = setInterval(function() {
                $('#timer').text(""+hours+" : "+mins+" : "+secs+"" );
                $('#timer-module').text(""+hours+" : "+mins+" : "+secs+"" );
                secs--;
                if(secs == 0){
                    if(mins == 0){
                        if(hours == 0){
                            clearInterval(testTime);
                            $('#timer').text("00 : 00 : 00" );
                            $('#timer-module').text("00 : 00 : 00" );
                            $('.anschecked').prop('disabled', true);
                            $('.mokanschecked').prop('disabled', true);
                            sweetAlert("Please submit your time is over");
                            disableAnswerFlag = true;
                            $("#next_section").hide();
                            
                        }
                        hours = hours - 1;
                        mins = 60;
                    }
                    mins = mins - 1;
                    secs = 60;
                }
            }, 1000);*/
        },
        
        nextSectionClick: function(){
            var totqueno = $(".quesClasses").length;
            $(".q"+count+"").hide();
            count ++;
            $(".q"+count+"").show();
            var nqs = queArray.length;
            
            $("#quesNo").text(""+(count+1)+"/"+nqs+"");
            if((count+1) == totqueno) {
                $("#next_section").hide();
                $("#submit_test").show();
            } else if(count == 1) {
                $("#back_section").show();
            }
        },
        
        preveiousSectionClick: function() {
            var totqueno = $(".quesClasses").length;
            $(".q"+count+"").hide();
            count --;
            $(".q"+count+"").show();
            var nqs = queArray.length;
            
            $("#quesNo").text(""+(count+1)+"/"+nqs+"");
            if(count == 0) {
                $("#back_section").hide();
            } else if((count+2) == totqueno) {
                $("#submit_test").hide();
                $("#next_section").show();
            }
        },
        
        finalTestSubmission: function() {
            for(var i=0; i< ansArray.length; i++){
                for(var j=0; j< queArray.length; j++){
                    if(ansArray[i].questionId == queArray[j].questionId){
                        if(ansArray[i].answer == queArray[j].answer){
                            correctAns++;
                        }
                    }
                }
            }
            participantroutes.navigate('test/conclusion', {trigger: true});
        },
        
        previewMockTestQuestion: function() {
            $("#preview-mock").modal({ keyboard: false });
            $("#preview-mock").modal('show');
            $("#preview-quession").empty();
            var totlQue = queArray.length;
            var answQue = 0;
            
            
            for(var i = 0; i < queArray.length; i++) {
                 $("#preview-quession").append(
                     '<button type="button" class="btn btn-circle btn-lg quesid'+queArray[i].questionId+' pre-que btn-danger" style="margin:2px;">'+(i+1)+'</button>'  
                 );
                for(var j=0; j< ansArray.length; j++){
                    if(queArray[i].questionId == ansArray[j].questionId) {
                        if( ansArray[j].answer != ""){
                            answQue++;
                            $("#preview-quession").children(".quesid"+queArray[i].questionId+"").removeClass('btn-danger').addClass('btn-success');
                        }
                    }
                }
             }
            $("#tot-que").text(totlQue);
            $("#ans-que").text(answQue);
        },
        openQuestionPanel: function(e){
            var quNo = e.currentTarget.classList;
            var pqs = e.currentTarget.textContent;
            var nqs = queArray.length;
            count = (pqs - 1);
            if(count > 0) {
                $("#back_section").show();
            } else {
                $("#back_section").hide();
            }
            if(pqs == nqs) {
                $("#next_section").hide();
            } else if (pqs < nqs) {
                $("#next_section").show();
            }
            $("#quesNo").text(""+pqs+"/"+nqs+"");
            $("#preview-mock").modal('hide');
            $(".quesClasses").hide();
            $($("#"+quNo[3]+"")).show();
        },
        
        
        mockTestSubmit: function(){
            $("#preview-mock").modal('hide');
            clearInterval(testTime);
            $("#pageloading").show();
            Backbone.ajax({
                url: "mok-testsubmit",
                data: JSON.stringify(ansArray),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 1) {
                        swal({
                            title: "Thank you, your answers are submitted",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        },
                             function(){
                            participantroutes.navigate('prepare/moc-conclusion', {trigger: true});
                        });
                    } else if(data == 2) {
                        swal({
                            title: "Problem in saving test, Please contanct IT admin",
                            confirmButtonColor: "#F01E1E",
                            confirmButtonText: "Ok"
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        /* FEEDBACK FORM FOR MOCK TEST */
    
        feedBackOfMockTest: function() {
            this.$el.html(
                '<div class="row">' +
                    '<div class="col-xs-6 col-xs-offset-3">' +
                        '<h3 class="page-header">You have completed the exam. Thank you.! </h3><br>' +
                        '<div class="panel-body">' +
                            '<div class="row">' +
                                '<div class="col-xs-12">' +
                                    '<div class="list-group">' +
                                        '<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Results</u></b> <label class="pull-right text-muted small result_fp"> 0 </label></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Module <label class="pull-right text-muted small module-name"> 0 </label></div> ' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Number Of Attempts <label class="no-of-attempts pull-right text-muted small"> 0 </label></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Total Number Of Questions<label class="total_question pull-right text-muted small"> 0 </label></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Number Of Answered Questions<label class="ans_question pull-right text-muted small"> 0 </label></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Number of Correct Answers <label class="correct-answer pull-right text-muted small"> 0 </label></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Passing Percentage <label class="pass-percent pull-right text-muted small"> 0 %</label></div>' +
                                        '<div class="list-group-item">&nbsp;&nbsp;Percentage Obtained <label class="your-percent pull-right text-muted small"> 0 %</label></div>' +
                                    '</div>' +
                                    '<a class="btn btn-danger" href="logout"><i class="fa fa-sign-out fa-fw"></i>Exit</a>' +
                                    '<button id="reviewAns" type="submit" class="btn btn-primary pull-right">Answers Review</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<!-- /.row -->'
            );
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "mok-scorecard",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                        var obtainedPercentage = parseFloat(((data.correctAns/data.totalQues)*100).toFixed(2));
                        $(".module-name").text(data.module);
                        $(".no-of-attempts").text(data.attempts);
                        $(".total_question").text(data.totalQues);
                        $(".ans_question").text(data.attemptQues);
                        $(".correct-answer").text(data.correctAns);
                        $(".pass-percent").text(""+data.passMarks+" %");
                        $(".your-percent").text(""+obtainedPercentage+" %");
                        if(data.compareAnswer==1){
                            $("#reviewAns").show();
                        } else {
                        	$("#reviewAns").hide();
                        }
                        if( obtainedPercentage >= parseFloat(data.passMarks)){
                            $(".result_fp").text("PASS");
                        } else {
                            $(".result_fp").text("FAIL");
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
    /* MOCK TEST REVIEW ANSWERS */
        mocTestReviewAnswer: function() {
            participantroutes.navigate('prepare/moc-reviewAnswer', {trigger: true});
        },
        
        reviewAnswerOfMockTest: function() {
            
            this.$el.html(
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<h1 class="page-header"> Review Answer <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>'+
                            '<div class="panel-body" style="max-height: 425px; overflow-y: scroll; padding: 25px;">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="panel-group" id="moc-testReview"></div>' +
                                    '</div>' +
                                '</div>' +
                                '<!-- /.row (nested) -->' +
                            '</div>' +
                            '<!-- /.panel-body -->' +
                        '</div>' +
                        '<!-- /.panel -->' +
                    '</div>' +
                    '<!-- /.col-xs-12 -->' +
                '</div>'
            );
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "mok-review",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        sweetAlert("please login again");
                    } else {
                        quesansElement = Backbone.$("#moc-testReview").empty();
                        for(var i=0; i< data.length; i++){
                            quesansElement.append(
                            '<div class="row rev-header" id=queanid'+data[i].qid+'>' +
                                '<div class="que-header"><b>'+(i+1)+'. &nbsp;&nbsp;</b><b>'+data[i].question+'?</b></div>' +
                                '<div class="col-xs-6">' +
                                    '<div>' +
                                        '<span class="ansopt1">1)&nbsp;&nbsp; '+data[i].opt1+'</span>' +
                                    '</div>' +
                                    '<div>' +
                                        '<span class="ansopt3">3)&nbsp;&nbsp; '+data[i].opt3+'</span>' +
                                    '</div>' + 
                                    '<div>' +
                                        '<span id="Opl5'+i+'" class="ansopt5">5)&nbsp;&nbsp; '+data[i].opt5+'</span>' +
                                    '</div>' + 
                                '</div>' +
                                '<div class="col-xs-6">' +
                                    '<div>' +
                                        '<span class="ansopt2">2)&nbsp;&nbsp; '+data[i].opt2+'</span>' +
                                    '</div>' +
                                    '<div>' +
                                        '<span class="ansopt4">4)&nbsp;&nbsp; '+data[i].opt4+'</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                            );
                            
                            if(data[i].opt5 == null){
                                $("#Opl5"+i+"").hide();
                            } else if(data[i].opt5 == 0){
                                $("#Opl5"+i+"").hide();
                            }
                            
                            if( data[i].answer.toLowerCase() == data[i].correctAnswer.toLowerCase() ){
                                $("#queanid"+data[i].qid+"> div > div > span.ans"+data[i].answer.toLowerCase()+"").css("color", "#00FF62");
                                $("#queanid"+data[i].qid+"> div:nth-child(3)").append('<span class="ansskip" style="color:#00be49">CORRECT</span>');
                            } else if ( data[i].answer.toLowerCase() == "" ) {
                                $("#queanid"+data[i].qid+"> div:nth-child(3)").append('<span class="ansskip" style="color:orange">SKIPPED</span>');
                                $("#queanid"+data[i].qid+"> div > div > span.ans"+data[i].correctAnswer.toLowerCase()+"").css("color", "#00FF62");
                            } else {
                                $("#queanid"+data[i].qid+"> div > div > span.ans"+data[i].correctAnswer.toLowerCase()+"").css("color", "#00FF62");
                                $("#queanid"+data[i].qid+"> div > div > span.ans"+data[i].answer.toLowerCase()+"").css("color", "#ff0000");
                                $("#queanid"+data[i].qid+"> div:nth-child(3)").append('<span class="ansskip" style="color:#ff0000">WRONG</span>');
                            }
                        }
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    });
})();
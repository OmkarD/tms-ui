var testroutes = Backbone.Router.extend({
    routes: {
        '': 'index',
        'test/:id': 'tests',
        'prepare/:id': 'preparingTest',
        'modules/:id/:moduName': 'moduleselect',
        'compare/:id': 'compareAnswer',
        '*other': 'default'
    },

    index: function () {
        this.tests("details");
    },
    
    moduleselect: function(id, mname) {
        participantview = new participant_details.Views.participants();
        $("#test-wrapper").empty().append(participantview.el);
        participantview.detailsOfParticipant(id, mname);
    },
    
    compareAnswer: function(id) {
        conclusionview = new participant_conclusion.Views.conclusion();
        $("#test-wrapper").empty().append(conclusionview.el);
        conclusionview.compareQueAnsDisplay();
    },
    
    preparingTest: function(id){
        
        $('.modal-backdrop.fade.in').hide();
        testview = new test_run.Views.testrun();
        $("#test-wrapper").empty().append(testview.el);
        if(id == "tpsingle"){
            singleQueFlag = true;
            multipleQueFlag = false;
            testview.render();
            
        } else if(id == "mtbeginsingle"){
            singleQueFlag = true;
            multipleQueFlag = false;
            testview.mockTestBegin();
            $("#instructions_page").modal({ keyboard: false });
            $("#instructions_page").modal('show');
            
        } else if(id == "tpmultiple"){
            multipleQueFlag = true;
            singleQueFlag = false;
            testview.render();
            
        } else if(id == "mtbeginmultiple"){
            multipleQueFlag = true;
            singleQueFlag = false;
            testview.mockTestBegin();
            
        } else if( id == "moc-conclusion"){
            testview.feedBackOfMockTest();
        } else if( id == "moc-reviewAnswer"){
            testview.reviewAnswerOfMockTest();
        }
    },
    
    tests: function(id) {
        
        $('.modal-backdrop.fade.in').hide();
        if (id == "details") {
            participantview = new participant_details.Views.participants();
            $("#test-wrapper").empty().append(participantview.el);
            participantview.render();
            
        } else if(id == "confirmtopictest"){
            participantview = new participant_details.Views.participants();
            $("#test-wrapper").empty().append(participantview.el);
            participantview.confirmTopicDetails();
            
        } else if(id == "confirmmocktest"){
            participantview = new participant_details.Views.participants();
            $("#test-wrapper").empty().append(participantview.el);
            participantview.confirmMockDetails();
            
        } else if(id == "conclusion"){
            conclusionview = new participant_conclusion.Views.conclusion();
            $("#test-wrapper").empty().append(conclusionview.el);
            conclusionview.render();
        }
    },

    default: function (other) {
        alert("Doesn't know this url " + other + "");
    }
});
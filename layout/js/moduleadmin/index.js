/*** Index  ***/
var dash_board, batch_manage, trainer_batch_manage, user_manage, config_module, trainer_manage, student_manage, admin_privileges, paper_create, program_module, question_master, test_paper, select_role, admin_attendmark, attend_mark, add_students;
var asTogal,heirarchyviewatmindex,heirarchyviewbtmindex,heirarchyviewbatchlist;

/*var addStudentCheckValue,addStudentCheck;*/
/*** Views File variable Declare General ***/
var progadminindex, dashboardindex, programmoduleindex, batchindex, trainerbatchindex, usermanageindex, trainermanageindex, heirarchyindex, studentmanageindex, adminprivindex, questionmasterindex, testpaperindex, moduleconfigindex, selectindex, attend_manage, attedenceindex, adminattendancemarkindex, admin_attedance, manageuserindex;

/*** Routes variable ***/
var batchroute, landingroutes, batchdetails, trainerbatchdetails, attedenceDetails, Alert, trainerLoginFlag, appAdminFlag = false, multipleQueFlag = false, singleQueFlag = false;
(function () {
    
    window.prog_admin = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
   /* $(document).attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none',  you could also put this in a class 
                   '-webkit-user-select':'none', and add the CSS class here instead 
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; }); 
    $("html").attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none',  you could also put this in a class 
                   '-webkit-user-select':'none', and add the CSS class here instead 
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; }); 
    $("*").attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none',  you could also put this in a class 
                   '-webkit-user-select':'none', and add the CSS class here instead 
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; }); 
    $("span").attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none',  you could also put this in a class 
                   '-webkit-user-select':'none', and add the CSS class here instead 
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; });*/
    
    /*** Routes for the Program admin Page ***/
    prog_admin.Routes.sidenav = sidenav;
    
    /*** Program Admin General View ***/
    prog_admin.Views.programe = programadmin;
    
    /*** Initializing Program admin index***/
    progadminindex = new prog_admin.Views.programe();
    $(document.body).append(progadminindex.el);

    /*** Initializing Program admin Routes ***/
    landingroutes = new prog_admin.Routes.sidenav();
    Backbone.history.start();
    
})();
(function (){
    
    var selCount = 0;
    var totCount = 0;
    var topicQueCount = [];
    window.test_paper = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    var wholeQuestions, prePareQuestions;
    
    test_paper.Views.header = Backbone.View.extend({
        
        events:{
            'focusout .takeinCls': 'interGerValidation',
            'click .allQuestionSelect': 'allQuestionSelect',
            'click .prepare_test': 'prepareMocTestQuestion',
            'change #test_language' :'changeToReset',
            'change #question_select':'listQuesForTest'
        },
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function(modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-xs-12">'+
                    '<h1 class="page-header">Test Management<div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>'+
                '</div>'+
                '<!-- /.col-xs-12 -->'+
            '</div>' +
            '<div id="que_test">' +
                '<div class="row">' +
                    '<div class="col-xs-3">' +
                        '<label>Select Language</label>' +
                    '</div>' +
                    '<div class="col-xs-3">' +
                       '<label>Select Question Bank Set</label>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-3">' +
                        '<select id="test_language" type="text" name="language-select" class="form-control" placeholder="Select Language" readonly></select>' +
                    '</div>' +
                   '<div class="col-xs-3">' +
                         '<select id="question_select" type="text" name="question-select" class="form-control" placeholder="Select Question" style="cursor: pointer;" readonly></select>' +
                    '</div>'+ 
                    '<div class="checkbox col-xs-4">' +
                        '<label>' +
                            '<input type="checkbox" class="allQuestionSelect"> Use all questions' +
                        '</label>' +
                    '</div>' +
                '</div>' +
                '<div class="row" id="testaftertable">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="testbeforetable" class="table table-hover">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Topic Name</b></td>' +
                                                '<td><b>Section Name</b></td>' +
                                                '<td><b>Total questions available : </b><input id="toQueCount" readonly tabindex="-1" style="width: 50px;"></td>' +
                                                '<td><b>Total questions selected : </b><input id="selQueCount" readonly tabindex="-1" style="width: 50px;"></td>' +
                                                '<td><button class="btn btn-outline btn-primary prepare_test pull-right" style="margin: 10px 0 10px 0;"  tabindex="2"> Prepare</button></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td colspan="5">' +
                                                    '<span class="errorTotal errorClss">' +
                                                        'Select question count can\'t be more than present question count' +
                                                    '</span>' +
                                                    '<span class="errorSelZero errorClss">' +
                                                        'Can\'t prepare question paper with zero(0) number of questions ' +
                                                    '</span>' +
                                                    '<span class="errorOnSubmit errorClss">' +
                                                        'Problem in saving data, Try again...! ' +
                                                    '</span>' +
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td><b>HIGH</b></td>' +
                                                '<td><b>MEDIUM</b></td>' +
                                                '<td><b>LOW</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                            '<div class="panel-footer">' +
                                '<button class="btn btn-outline btn-primary prepare_test pull-right" style="margin: 10px 0 10px 0;"  tabindex="2"> Prepare</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>');
            testpaperindex.DispLangToSelect();
        },
        
        
        
        interGerValidation: function(e) {
            selCount = 0;
            $(".errorClss").hide();
            $(".preparError").remove();
            var existId = e.currentTarget.id.replace("ent", "ext");
            if($("#"+e.currentTarget.id+"").val().length >= 1){
                $(e.currentTarget).css({'background-color': '#FFF'})
                if(!$.isNumeric($("#"+e.currentTarget.id+"").val())){
                    var myStr = $("#"+e.currentTarget.id+"").val();
                    myStr = myStr.replace(/\D+/g, '');
                    $(e.currentTarget.parentElement).append('<div class="errorClss preparError"><br>It should be an Integer</div>');
                    $(".preparError").show();
                    $("#"+e.currentTarget.id+"").val(myStr);
                    if($("#"+e.currentTarget.id+"").val() == ""){
                        $("#"+e.currentTarget.id+"").val(0);
                    }
                    
                } else if( parseInt($("#"+e.currentTarget.id+"").val()) > parseInt($("#"+existId+"").text()) ){
                    $(e.currentTarget.parentElement).append('<div class="errorClss preparError"><br>The value should be less than  '+$("#"+existId+"").text()+'</div>');
                    $(".preparError").show();
                    $("#"+e.currentTarget.id+"").val(0);
                    
                } else if(parseInt($("#"+e.currentTarget.id+"").val()) < 0){
                    var myStr = $("#"+e.currentTarget.id+"").val();
                    myStr = myStr.slice(0, -1);
                    $(e.currentTarget.parentElement).append('<div class="errorClss preparError"><br>The value cannot be Negative</div>');
                    $(".preparError").show();
                    $("#"+e.currentTarget.id+"").val(0);
                }
            } else if($("#"+e.currentTarget.id+"").val() == ""){
                $("#"+e.currentTarget.id+"").val(0);
            }
            
            for( var i=0 ; i< $(".takeinCls").length; i++){
                selCount = selCount + parseInt($($(".takeinCls")[i]).val());
                $("#selQueCount").val(selCount);
            }
        },
        
    /*** GENERATE THE QUESTION LIST ALREADY IN ***/
        DispLangToSelect: function() {
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "modlanglist",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        Backbone.$("#test_language").empty();
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#test_language").append(
                                '<option value='+data[i].langId+' class="languageCls">'+data[i].shortDesc+'</option>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                    testpaperindex.DispQuestionBankToSelect(mid);
                }
            });
        },
        
        
        
        
        DispQuestionBankToSelect: function(mid) {
            $("#pageloading").show(); 

            Backbone.ajax({
                dataType: "json",
                url: "listqbsbymodid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        Backbone.$("#question_select").empty();
                        for (var i=0; i< data.length; i++){
                            if(i == 0){
                              Backbone.$("#question_select").append(
                                '<option value="0">Select Question Bank Set</option>'
                                 );
                     }

                            Backbone.$("#question_select").append(
                     '<option value='+data[i].qbsId+' id="questionbankset">'+data[i].shortDesc+'</option>' 
                                    
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                    
                }
            });
        },


        
    /*List the Number of Questions*/
        listQuesForTest: function() {
        	  var quid=  $("#question_select").val ();
        	 $("#testaftertable").show();
            var lnid = $("#test_language").val();
            $(".allQuestionSelect").prop('checked',false);
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listtopicwithcount?langId="+lnid+"&qbsId="+quid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    selCount = 0;
                    totCount = 0;
                    if(data == 0){
                        
                    } else{
                        wholeQuestions = Backbone.$("#testbeforetable").children("tbody").empty();
                        testpaperindex.generateQuesTopicCount(data);
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        generateQuesTopicCount: function(data) {
            
            for (var i=0; i< data.length; i++){
                wholeQuestions.append('<tr><td>'+data[i].shortDesc+'</td>' +
                                          '<td></td>' +
                                          '<td></td>' +
                                          '<td></td>' +
                                          '<td></td>' +
                                      '</tr>');
                var sObj = data[i].sectionDetailsForms;
                var topid = data[i].uniId;
                testpaperindex.generateQuesSectionCount(sObj, topid);
            }
            
        },
        
        generateQuesSectionCount: function(sObj, topid){
            for (var j=0; j< sObj.length; j++){
                var sesid = sObj[j].uniId;
                wholeQuestions.append(
                    '<tr>' +
                        '<td></td>' +
                        '<td>'+sObj[j].sectionDesc+'</td>' +
                        '<td><input id="ent-'+topid+'-'+sesid+'-H" class="takeinCls" tabindex="1"><span class="divident1">/</span><span class="divident2">/</span><span id="ext-'+topid+'-'+sesid+'-H" class="existCls" tabindex="-1"></span></td>' +
                        '<td><input id="ent-'+topid+'-'+sesid+'-M" class="takeinCls" tabindex="1"> <span class="divident1">/</span><span class="divident2">/</span><span id="ext-'+topid+'-'+sesid+'-M" class="existCls" tabindex="-1"><span></td>' +
                        '<td><input id="ent-'+topid+'-'+sesid+'-L" class="takeinCls" tabindex="1"><span class="divident1">/</span><span class="divident2">/</span><span id="ext-'+topid+'-'+sesid+'-L" class="existCls" tabindex="-1"></span></td>' +
                    '</tr>');
                var leObj = sObj[j].levelMasterForms;
                var setoid = ""+topid+"-"+sesid+"";
                testpaperindex.generateQuesLevelCount(leObj, setoid);
            }
        },
        
        generateQuesLevelCount: function(leObj, stid){
            for (var k=0; k< leObj.length; k++){
                totCount = totCount + parseInt(leObj[k].questionCount);
                selCount = selCount + parseInt(leObj[k].selectedCount);
                
                var allSelec = ""+stid+"-"+leObj[k].shortDesc+"";
                topicQueCount.push({"topicCountId":allSelec,"selQueCount":leObj[k].selectedCount});
                $("#toQueCount").val(totCount);
                $("#selQueCount").val(selCount);
                if(leObj[k].shortDesc == "H"){
                    $("#ext-"+stid+"-H").text(leObj[k].questionCount);
                    $("#ent-"+stid+"-H").val(leObj[k].selectedCount);
                } else if(leObj[k].shortDesc == "M"){
                    $("#ext-"+stid+"-M").text(leObj[k].questionCount);
                    $("#ent-"+stid+"-M").val(leObj[k].selectedCount);
                } else if(leObj[k].shortDesc == "L"){
                    $("#ext-"+stid+"-L").text(leObj[k].questionCount);
                    $("#ent-"+stid+"-L").val(leObj[k].selectedCount);
                }
            }
        },
        
        prepareMocTestQuestion: function(){
            var json = [];
            var pl = $(".takeinCls").length;
            $(".errorClss").hide();
            for( var i=0; i<pl; i++){
                if(parseInt($("#"+$(".existCls")[i].id+"").text()) < parseInt($("#"+$(".takeinCls")[i].id+"").val())){
                    $("#"+$(".takeinCls")[i].id+"").css({'background-color': '#F01E1E'});
                    $(".errorTotal").show();
                } else {
                    $("#"+$(".takeinCls")[i].id+"").css({'background-color': '#FFF'});
                    var val = $(".takeinCls")[i].id.split('-');
                    var topId = val[1];
                    var sesId = val[2];
                    var levId = val[3];
                    var queC = $("#"+$(".takeinCls")[i].id+"").val();
                    try{
                        json.push({"topicId": topId,"sectionId":sesId,"levelId":levId,"questionCount":queC});
                    } catch(e){}
                }
            }
            if(json.length == pl){
                $(".errorClss").hide();
                var sc = $("#selQueCount").val();
                if( sc == 0){
                    $(".errorSelZero").show();
                }  else {
                    var tc = $("#toQueCount").val();

                    swal({
                        title: "You have selcted "+sc+" questions out of "+tc+"",
                        confirmButtonColor: "#F01E1E",
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        cancelButtonText: "No"
                    },
                         function(){
                        $("#pageloading").show();
                        Backbone.ajax({
                            url: "testrulesave",
                            data: JSON.stringify(json),
                            type: "POST",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader(header, token);
                                xhr.setRequestHeader("Accept", "application/json");
                                xhr.setRequestHeader("Content-Type", "application/json");
                            },
                            success: function(data) {
                                
                                if(data == 1){
                                    $(".errorClss").hide();
                                    testpaperindex.listQuesForTest();
                                } else if(data == 2){
                                    $(".errorOnSubmit").show();
                                }
                            },
                            error:function(res,ioArgs){
                                if(res.status == 403){
                                    window.location.reload();
                                }
                            },
                complete: function() {
                    $("#pageloading").hide();
                    sweetAlert("Test paper format submitted successfully");
                }
                        });
                    });
                }
            }
        },
        
        allQuestionSelect: function() {
            if($(".allQuestionSelect").is(':checked')){
                $(".takeinCls").prop('disabled', true);
                $("#selQueCount").val(totCount);
                for(var i = 0; i < topicQueCount.length; i++){
                    $("#ent-"+topicQueCount[i].topicCountId+"").val($("#ext-"+topicQueCount[i].topicCountId+"").text());
                }
            } else {
                $(".takeinCls").prop('disabled', false);
                $("#selQueCount").val(0);
                for(var i = 0; i < topicQueCount.length; i++){
                    $("#ent-"+topicQueCount[i].topicCountId+"").val(0);
                }
            }
        }
    });
})();

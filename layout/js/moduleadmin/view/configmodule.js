(function (){
    
    var configElement, mid, languageElement, topicElement;
    window.config_module = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    config_module.Views.header = Backbone.View.extend({
        
        events: {
            'keyup .integervalid': 'testTimingValidation',
            'focusout #passPercent': 'testTimingValidation',
            'focusout #batchExtension':'testTimingValidation',
            /*'click .enableBatchExtensionCheck':'enableBatchExtension',*/
            'change .batchExtensionCheck':'noOfBatchExtension',
            'click #configurationsubmit': 'moduleConfigVerify',
            'click .testCheck': 'noOfAttemptToggle',
            'click .new_section': 'addANewsectionToApp',
            'click .appsectionsubmit': 'submitNewSectionToApp',
            'keyup #sect_id': 'specialCharValidate',
            'click .addslots': 'addSlotModal',
            'click #addNewTopic': 'newTopicForModule',
            'click #addNewQuestionBank':'newQuestionBankForModule',
            'click #addNewLanguage': 'newLanguageForModule',
            'click .newquestionbank_submit':'submitNewQuestionBankForModule',
            'click .newtopic_submit': 'submitNewTopicForModule',
            'click .newlanguage_submit': 'submitNewLangForModule',
            'click .editTopic': 'editTopicForModule',
            'click .editquestionbank':'editQuestionBankForModule',
            
            'click .editquestionbank_submit': 'submitEditQuestionBankForModule',
            
            'click .edittopic_submit': 'submitEditTopicForModule',
            'click #slotAddModule': 'addSlotToModule',
            'click .slotCancel': 'reloadExistingPage',
            'click .slot-delete': 'deleteSlotsFromModule',
            'mousedown .instrutfile': 'beforeImgUpload',
            'change .instrutfile': 'instrutionImgUpload'
        },
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function (modulid) {
            mid = modulid;
            $("#sidebar").empty().append(
                '<!-- /.navbar-top-links -->' +
                '<div class="navbar-default sidebar" role="navigation">' +
                    '<div class="sidebar-nav navbar-collapse">' +
                        '<ul class="nav" id="side-menu">' +
                            '<li id="config_module" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Module Configuration">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i>' +
                                '<span class="content-hide">Configuration</span>' +
                            '</li>' +
                            '<li id="user_manage" class="maind" data-toggle="tooltip tab" data-original-title="User Management">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i>' +
                                '<span class="content-hide">User Management</span>' +
                            '</li>' +
                            '<li id="batchmanagement" class="maind" data-toggle="tooltip tab" data-original-title="Batches Management">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>' +
                                '<span class="content-hide">Batch Management</span>' +
                            '</li>' +
                            '<li id="ques_master" class="maind" data-toggle="tooltip tab" data-original-title="Question Bank">&nbsp;&nbsp;<i class="fa fa-pencil fa-fw"></i>' +
                                '<span class="content-hide">Question Bank</span>' +
                            '</li>' +
                            '<li id="admin_attedance" class="maind" data-toggle="tooltip tab" data-original-title="Attendance Management">&nbsp;&nbsp;<i class="fa fa-calendar fa-fw"></i>' +
                                '<span class="content-hide">Attendance Management</span>' +
                            '</li>' +
                            '<li id="paper_create" class="maind" data-toggle="tooltip tab" data-original-title="Test Management">&nbsp;&nbsp;<i class="fa fa-sitemap fa-fw"></i>' +
                                '<span class="content-hide">Test Management</span>' +
                            '</li>' +
                            '<li class="maindashboard">' +
                                '<span id="menu-toggle" href="#menu-toggle"></span>' +
                            '</li>' +
                        '</ul>' +
                    '</div>' +
                    '<!-- /.sidebar-collapse -->' +
                '</div>' +
                '<!-- /.navbar-static-side -->'
                );
             if(asTogal==1){
                    Backbone.$(".sidebar-nav").toggleClass("toggled");
                    Backbone.$("#menu-toggle").toggleClass("toggled");
                    Backbone.$("#page-wrapper").toggleClass("toggled");
                    Backbone.$(".content-hide").toggleClass("toggled");
                    Backbone.$(".sidebar-nav li").toggleClass("toggled");
                    Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
            }
            this.$el.empty().append(
       /*     '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<h1 class="page-header"> Module Configuration  <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                '</div>'+
                '<!-- /.col-xs-12 -->' +
            '</div>' +
            '<div class="row">' +
                '<div class="col-xs-12">' +
                    '<div class="panel">' +
                        '<div class="panel-heading">' +
                            '<h4>Module Configuration Details</h4>' +
                        '</div>' +
                        '<div class="panel-body">' +
                            '<div class="table-responsive">' +
                                '<table id="configmoduletable" class="table table-hover" style="text-align:left;">' +
                                    '<tbody>' +
                                        '<tr class="integerError errorClss">' +
                                            '<td colspan="3"> It should be an Integer </td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td><b> Enable Test </b></td>' +
                                            '<td>' +
                                                '&nbsp;<input type="checkbox" name="module-test" class="testCheck" style="vertical-align: middle;">' +
                                            '</td>' +
                                            '<td></td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td><b>Enable Training</b></td>' +
                                            '<td>' +
                                                '&nbsp;<input type="checkbox" name="module-training" class="trainingCheck" style="vertical-align: middle;">' +
                                            '</td><td>' +
                                                
                                            '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                        '<td><b>Enable Answer View</b></td>' +
                                        '<td>' +
                                            '&nbsp;<input type="checkbox" name="module-training" class="compAnsCheck" style="vertical-align: middle;">' +
                                        '</td><td>' +
                                            
                                        '</td>' +
                                    '</tr>' +
                                    '<tr>'+
                                    '<tr>'+
                                    ' <td><b>Enable TCC Status</b></td>'+
                                    '<td>' +
                                     '&nbsp;<input type="checkbox" name="tcc-status" class="tccStatusCheck" style="vertical-align: middle;">'+
                                     '</td><td>'+
                                     '</td>'+
                                    '</tr>'+
                                    '</tr>'+
                                    
                                    
                                    '<tr>'+
                                    '<tr>'+
                                     ' <td><b>Preferred Date of Exam</b></td>' +
                                     '<td>'+
                                     '&nbsp;<input type="checkbox" name="pref-date-Of-Exam" class="prefDateOfExamCheck" style="vertical-align: middle;">'+
                                      '</td><td>'+
                                      '</td>'+
                                      '</tr>'+
                                      '</tr>'+
                                
                                      '<tr>'+
                                      '<tr>'+
                                      '<td><b>Assesment/Mock test expiry</b></td>' +
                                      '<td>'+
	                                      '<div class="radio">' +
	                                           '<label><input type="radio" class="batchExtensionCheck" name="optRadio" value="0" checked>BATCH EXPIRY</label>' +
	                                       '</div>' +
	                                       '<div class="radio">' +
	                                           '<label><input type="radio" class="batchExtensionCheck" name="optRadio" value="1">DAYS POST BATCH EXPIRY</label>' +
	                                       '</div>'+
                                        '</td>'+
                                        '</tr>'+
                                        '</tr>'+
                                        
                                        '<tr id="batchExtensionhide">' +
                                        '<td><b>Batch Extended days</b></td>' +
                                        '<td>' +
                                        '&nbsp;<input id="batchExtension" type="Number" min="0" name="batch-extension" class="integervalid" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" style="vertical-align: middle;">' +
                                        '</td>' +
                                        '<td></td>' +
                                    '</tr>' +
                                     
                                     '<tr>' +
                                     '<td><b>Minimum Number of Attendance Count</b></td>' +
                                     '<td>' +
                                     '&nbsp;<input id="minimumattendance"  type="Number" min="0"  onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" name="minimum-attendance-count" class="integervalid">'  +
                                     '</td>' +
                                     '<td></td>' +
                                     '</tr>' +
                                        '<tr class="nooftestattements">' +
                                            '<td><b>Test Duration </b>(Minutes)</td>' +
                                            '<td>' +
                                                '<input id="testtiming" type="text" name="maximum-test-time" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" class="integervalid"> Mins' +
                                            '</td>' +
                                            '<td></td>' +
                                        '</tr>' +
                                        '<tr class="errorTestDuration errorClss">' +
                                            '<td colspan="3"> Add Test Duration in Minutes </td>' +
                                        '</tr>' +
                                        '<tr class="nooftestattements">' +
                                            '<td><b>No of attempts</b></td>' +
                                            '<td>' +
                                                '<input id="testAttempt" type="text" name="test-Attempts" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" class="integervalid">' +
                                            '</td>' +
                                            '<td></td>' +
                                        '</tr>' +
                                        '<tr class="errorMaxAtm errorClss">' +
                                            '<td colspan="3"> Add Maximum Number of Test Attempts </td>' +
                                        '</tr>' +
                                        '<tr class="nooftestattements">' +
                                            '<td><b>Question display</b></td>' +
                                            '<td>'+ 
                                                '<input type="radio" name="multiple" class="multiCheck" value="1"> Single &nbsp;&nbsp; ' +
                                                '<input type="radio" name="multiple" class="multiCheck" value="2"> Multiple' +
                                            '</td>' +
                                            '<td></td>' +
                                        '</tr>' +
                                        '<tr class="errorMultiCheck errorClss">' +
                                            '<td colspan="3">Select Question Display Type </td>' +
                                        '</tr>' +
                                        '<tr class="nooftestattements">' +
                                            '<td><b>Pass Marks </b>(Percentage)</td>' +
                                            '<td>'+ 
                                                '<input type="text" name="pass-percent" id="passPercent" class="integervalid"> %' +
                                            '</td>' +
                                            '<td></td>' +
                                        '</tr>' +
                                        '<tr class="nooftestattements">' +
                                            '<td><b>Practice Test </b></td>' +
                                            '<td>'+ 
                                                '&nbsp;<input type="radio" name="module-practicetest" class="practiceTest" value="1"> Required &nbsp;&nbsp;' +
                                                '&nbsp;<input type="radio" name="module-practicetest" class="practiceTest" value="2"> Not Required' +
                                            '</td>' +
                                            '<td></td>' +
                                        '</tr>' +
                                        '<tr class="errorPracticeTest errorClss">' +
                                            '<td colspan="3">Please Provide Information of Practice Test Requied or Not </td>' +
                                        '</tr>' +
                                        '<tr class="errorTestPassPercent errorClss">' +
                                            '<td colspan="3">Add Passing Marks in Percentage</td>' +
                                        '</tr>' +
                                        '<tr class="errorTestPassPercentGreter errorClss">' +
                                            '<td colspan="3">Percentage is less than equal to 100</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td><span><b>Module Languages: </b></span></td>' +
                                            '<td><div id="language_select"></div></td>' +
                                            '<td><button id="addNewLanguage" class="btn btn-default">Add Language</button></td>' +
                                        '</tr>' +
                                        '<tr class="errorClss errorOnAddLanguage">' +
                                            '<td colspan="3"> Adding Language Failed. Try Again...! </td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td><span><b>Module Topics: </b></span></td>' +
                                            '<td><div id="topic_select"></div></td>' +
                                            '<td><button id="addNewTopic" class="btn btn-default">Add New Topic</button></td>' +
                                        '</tr>' +
                                        '<tr>' +
                                        '<td><span><b>Module Question Bank Set: </b></span></td>' +
                                        '<td><div id="questionbank_select"></div></td>' +
                                        '<td><button id="addNewQuestionBank" class="btn btn-default">Add Question Bank</button></td>' +
                                       '</tr>' +
                                        '<tr>' +
                                            '<td><span><b>Module Slots: </b></span></td>' +
                                            '<td><div id="slots-available"></div></td>' +
                                            '<td><button class="addslots btn btn-default">Add New Slot</button></td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td><span><b>Module Sections: </b></span></td>' +
                                            '<td><div id="section_list"></div></td>' +
                                            '<td><button class="btn btn-default new_section">Add New Section</button></td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td><span><b>Instruction Upload: </b></span></td>' +
                                            '<td><select id="lang-inst" class="form-control"></select></td>' +
                                            '<td>' +
                                                '<div class="col-xs-1">' +
                                                    '<form method="post" enctype="multipart/form-data">'+
                                                        '<div class="btn btn-outline btn-primary btn-file">'+
                                                            '<i class="fa fa-upload">'+
                                                            '</i> &nbsp;Upload <input name="filename" type="file" class="instrutfile" accept="image/jpeg" style="left: 16px;width: 90px;">'+
                                                        '</div>'+
                                                    '</form>'+
                                                '</div>'+
                                            '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '<td colspan="3"><div class="integerError errorClss">It should be an Integer</div><div class="errorTestStatus errorClss"> Please check test status </div><div class="errorInstUploadFail errorClss">Instruction upload failed</div></td>' +
                                        '</tr>' +
                                    '</tbody>' +
                                '</table>' +
                            '</div>' +
                        '</div>' +
                        '<div class="panel-footer">' +
                            '<div class="row">' +
                                '<div class="col-xs-11">' +
                                    '<div class="errorClss errorOnConfigSave">Module Configuration Not Saved. Try Again...!</div>' +
                                '</div>' +
                                '<div class="col-xs-1">' +
                                    '<button id="configurationsubmit" class="btn btn-outline btn-primary"> Save </button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            */
            
            /*------------------------------Modified Configuration--------------------------*/
            
                    '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<h1 class="page-header"> Module Configuration  <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>' +
                    '</div>'+
                    '<!-- /.col-xs-12 -->' +
                '</div>' +
                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-heading">' +
                                '<h4>Module Configuration Details</h4>' +
                            '</div>' +
                            '<div class="panel-body">' +
                            '<div class="configborder row">'+
                               '<div class="configcheckbox col-xs-12 col-lg-6">'+
                                  '<div class="table-responsive">' +
                                    '<table id="configmoduletable" class="table table-hover" style="text-align:left;">' +
                                        '<tbody>' +
                                            '<tr class="integerError errorClss">' +
                                                '<td colspan="3"> It should be an Integer </td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b> Enable Test </b></td>' +
                                                '<td>' +
                                                    '&nbsp;<input type="checkbox" name="module-test" class="testCheck" style="vertical-align: middle;">' +
                                                '</td>' +
                                                '<td></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><b>Enable Training</b></td>' +
                                                '<td>' +
                                                    '&nbsp;<input type="checkbox" name="module-training" class="trainingCheck" style="vertical-align: middle;">' +
                                                '</td><td>' +
                                                    
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                            '<td><b>Enable Answer View</b></td>' +
                                            '<td>' +
                                                '&nbsp;<input type="checkbox" name="module-training" class="compAnsCheck" style="vertical-align: middle;">' +
                                            '</td><td>' +
                                                
                                            '</td>' +
                                        '</tr>' +
                                        '<tr>'+
                                        '<tr>'+
                                        ' <td><b>Enable TCC Status</b></td>'+
                                        '<td>' +
                                         '&nbsp;<input type="checkbox" name="tcc-status" class="tccStatusCheck" style="vertical-align: middle;">'+
                                         '</td><td>'+
                                         '</td>'+
                                        '</tr>'+
                                        '</tr>'+
                                        
                                        
                                        '<tr>'+
                                        '<tr>'+
                                         ' <td><b>Preferred Date of Exam</b></td>' +
                                         '<td>'+
                                         '&nbsp;<input type="checkbox" name="pref-date-Of-Exam" class="prefDateOfExamCheck" style="vertical-align: middle;">'+
                                          '</td><td>'+
                                          '</td>'+
                                          '</tr>'+
                                          '</tr>'+
                                    
                                          '<tr>'+
                                          '<tr>'+
                                          '<td><b>Assessment/Mock test expiry</b></td>' +
                                          '<td>'+
    	                                      '<div class="radio">' +
    	                                           '<label><input type="radio" class="batchExtensionCheck" name="optRadio" value="0" checked>BATCH EXPIRY</label>' +
    	                                       '</div>' +
    	                                       '<div class="radio">' +
    	                                           '<label><input type="radio" class="batchExtensionCheck" name="optRadio" value="1">DAYS POST BATCH EXPIRY</label>' +
    	                                       '</div>'+
                                            '</td>'+
                                            '</tr>'+
                                            '</tr>'+
                                        '</tbody>' +
                                    '</table>' +
                                  '</div>'+
                                '</div>'+
                                '<div class="configtextbox col-xs-12 col-lg-6">'+
                                  '<div class="table-responsive">' +
                                    '<table id="config" class="table table-hover" style="text-align:left;">' +
                                        '<tbody>' +
                                            '<tr id="batchExtensionhide">' +
                                            '<td ><b>Batch Extended days</b></td>' +
                                            '<td>' +
                                            '&nbsp;<input id="batchExtension" type="text" min="0" name="batch-extension" class="integervalid " style="vertical-align: middle;border: 3px solid #555;" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57">' +
                                            '</td>' +
                                            '<td></td>' +
                                            '</tr>' +
                                         
                                            '<tr>' +
                                            '<td><b>Minimum Number of Attendance Count</b></td>' +
                                            '<td>' +
                                            '&nbsp;<input id="minimumattendance"  type="Number" min="0"  name="minimum-attendance-count" class="integervalid" style="border: 3px solid #555;border-style: inset;"onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57">'  +
                                            '</td>' +
                                            '<td></td>' +
                                            '</tr>' +
                                            '<tr class="nooftestattements">' +
                                                '<td><b>Test Duration </b>(Minutes)</td>' +
                                                '<td>' +
                                                    '<input id="testtiming" type="text" style="border: 3px solid #555;border-style: inset;" name="maximum-test-time" class="integervalid" >'+
                                                '</td>' +
                                                '<td></td>' +
                                            '</tr>' +
                                            '<tr class="errorTestDuration errorClss">' +
                                                '<td colspan="3"> Add Test Duration in Minutes </td>' +
                                            '</tr>' +
                                            '<tr class="nooftestattements">' +
                                                '<td><b>No of attempts</b></td>' +
                                                '<td>' +
                                                    '<input id="testAttempt" type="text" name="test-Attempts" style="border: 3px solid #555;border-style: inset;" class="integervalid" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57">' +
                                                '</td>' +
                                                '<td></td>' +
                                            '</tr>' +
                                            '<tr class="errorMaxAtm errorClss">' +
                                                '<td colspan="3"> Add Maximum Number of Test Attempts </td>' +
                                            '</tr>' +
                                            '<tr class="nooftestattements">' +
                                                '<td><b>Question display</b></td>' +
                                                '<td>'+ 
                                                    '<input type="radio" name="multiple" class="multiCheck" value="1"> Single &nbsp;&nbsp; ' +
                                                    '<input type="radio" name="multiple" class="multiCheck" value="2"> Multiple' +
                                                '</td>' +
                                                '<td></td>' +
                                            '</tr>' +
                                            '<tr class="errorMultiCheck errorClss">' +
                                                '<td colspan="3">Select Question Display Type </td>' +
                                            '</tr>' +
                                            '<tr class="nooftestattements">' +
                                                '<td><b>Pass Marks </b>(%)</td>' +
                                                '<td>'+ 
                                                    '<input type="text" name="pass-percent" style="border: 3px solid #555;border-style: inset;"id="passPercent" class="integervalid">'+
                                                '</td>' +
                                                '<td></td>' +
                                            '</tr>' +
                                            '<tr class="errorTestPassPercent errorClss">' +
                                            '<td colspan="3">Add Passing Marks in Percentage</td>' +
                                        '</tr>' +
                                        '<tr class="errorTestPassPercentGreter errorClss">' +
                                            '<td colspan="3">Percentage is less than equal to 100</td>' +
                                        '</tr>' +
                                             '<tr class="nooftestattements">' +
                                                '<td><b>Practice Test </b></td>' +
                                                '<td>'+ 
                                                    '&nbsp;<input type="radio" name="module-practicetest" class="practiceTest" value="1"> Required &nbsp;&nbsp;' +
                                                    '&nbsp;<input type="radio" name="module-practicetest" class="practiceTest" value="2"> Not Required' +
                                                '</td>' +
                                                '<td></td>' +
                                            '</tr>' +
                                            '<tr class="errorPracticeTest errorClss">' +
                                            '<td colspan="3">Please Provide Information of Practice Test Requied or Not </td>' +
                                        '</tr>' +
                                        '</tbody>' +    
                                     '</table>' +
                                     '</div>' +
                                     '</div>' +
                                 '</div>'+
                                '</div>' +
                             '</div>'+
                         '</div>' +
                     '</div>' +

                                    '<div class="row">' +
                                       '<div class="col-xs-11">' +
                                          '<div class="errorClss errorOnConfigSave">Module Configuration Not Saved. Try Again...!</div>' +
                                       '</div>' +
                                       '<div class="col-xs-2" style="padding-bottom: 20px;">' +
                                          '<button id="configurationsubmit" style="margin-left:525px;margin-top:0px;" class="btn btn-outline btn-primary"> Save </button>' +
                                       '</div>' +
                                   '</div>' +
                                  
                              

                '<div class="row">' +
                    '<div class="col-xs-12">' +
                        '<div class="panel">' +
                            '<div class="panel-heading">' +
                                '<h4>Other Configuration Details</h4>' +
                            '</div>' +
                            '<div class="panel-body">' +
                             '<div class="table-responsive">' +
                               '<div class="configaddbox" style="border-style: ridge;">'+
                                   '<table id="configtable" class="table table-hover">' +
                                        '<tbody>' +
                                           
                                         
                                            '<tr>' +
                                                '<td><span><b>Module Languages: </b></span></td>' +
                                                '<td><div id="language_select"></div></td>' +
                                                '<td><button id="addNewLanguage" class="btn btn-default">Add Language</button></td>' +
                                            '</tr>' +
                                            '<tr class="errorClss errorOnAddLanguage">' +
                                                '<td colspan="3"> Adding Language Failed. Try Again...! </td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><span><b>Module Topics: </b></span></td>' +
                                                '<td><div id="topic_select"></div></td>' +
                                                '<td><button id="addNewTopic" class="btn btn-default">Add New Topic</button></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><span><b>Module Slots: </b></span></td>' +
                                                '<td><div id="slots-available"></div></td>' +
                                                '<td><button class="addslots btn btn-default">Add New Slot</button></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><span><b>Module Sections: </b></span></td>' +
                                                '<td><div id="section_list"></div></td>' +
                                                '<td><button class="btn btn-default new_section">Add New Section</button></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                               '<td><span><b>Module Question Bank Set: </b></span></td>' +
                                               '<td><div id="questionbank_select"></div></td>' +
                                               '<td><button id="addNewQuestionBank" class="btn btn-default">Add Question Bank</button></td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td><span><b>Instruction Upload: </b></span></td>' +
                                                '<td><select id="lang-inst" class="form-control"></select></td>' +
                                                '<td>' +
                                                    '<div class="col-xs-1">' +
                                                        '<form method="post" enctype="multipart/form-data">'+
                                                            '<div class="btn btn-outline btn-primary btn-file">'+
                                                                '<i class="fa fa-upload">'+
                                                                '</i> &nbsp;Upload <input name="filename" type="file" class="instrutfile" accept="image/jpeg" style="left: 16px;width: 90px;">'+
                                                            '</div>'+
                                                        '</form>'+
                                                    '</div>'+
                                                '</td>' +
                                            '</tr>' +
                                            '<tr>' +
                                                '<td colspan="3"><div class="integerError errorClss">It should be an Integer</div><div class="errorTestStatus errorClss"> Please check test status </div><div class="errorInstUploadFail errorClss">Instruction upload failed</div></td>' +
                                            '</tr>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>'+
                              '</div>' +
                            '</div>'+
                        '</div>'+
                    '</div>' +
                '</div>' +
            
              /*------------------------------------------End----------------------*/
            '<div class="modal fade" id="createnewtopic" tabindex="-1" role="dialog" aria-labelledby="createnewtopic" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Create topic for this module </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="table-responsive" style="max-height:300px; overflow:scroll;">' +
                                            '<table id="topics_table" class="table table-hover" style="text-align:center">' +
                                                '<thead>' +
                                                    '<tr>' +
                                                        '<th> Topics </th>' +
                                                        '<th></th>' +
                                                    '</tr>' +
                                                '</thead>' +
                                                '<tbody>' +
                                                '</tbody>' +
                                            '</table>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="form-group col-xs-11">' +
                                            '<label for="topicname" class="control-label">Topic Name</label><span class="clsRed">*</span>' +
                                            '<input id="topicname" type="text" name="topic-name" class="form-control">' +
                                            '<span class="errortopicname errorClss">Please type the topic name</span>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<span class="errorinsubmit errorClss pull-left">Problem On Submit. Try Later..!</span>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div class="newtopic_submit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            
            
            
            '<div class="modal fade" id="createnewquestionbank" tabindex="-1" role="dialog" aria-labelledby="createnewquestionbank" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content" style="border:10px solid #003264">' +
                    '<div class="modal-header">' +
                        '<h4 class="modal-title"> Create question bank for this module </h4>' +
                    '</div>' +
                    '<div class="modal-body">' +
                        '<div class="panel-body">' +
                            '<div class="row">' +
                                '<div class="col-xs-12">' +
                                    '<div class="table-responsive" style="max-height:300px; overflow:scroll;">' +
                                        '<table id="questionbank_table" class="table table-hover" style="text-align:center">' +
                                            '<thead>' +
                                                '<tr>' +
                                                    '<th> Question Bank Set </th>' +
                                                    '<th></th>' +
                                                '</tr>' +
                                            '</thead>' +
                                            '<tbody>' +
                                            '</tbody>' +
                                        '</table>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12">' +
                                    '<div class="form-group col-xs-11">' +
                                        '<label for="questionbankset" class="control-label">Question Bank SetName</label><span class="clsRed">*</span>' +
                                        '<input id="questionbankset" type="text" name="quetionbank-set" class="form-control">' +
                                        '<span class="errorquestionbankset errorClss">Please type the Question Bank name</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                        '<span class="errorinsubmit errorClss pull-left">Problem On Submit. Try Later..!</span>' +
                        '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                        '<div class="newquestionbank_submit btn btn-default"> Submit </div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        
        
        
            '<div class="modal fade" id="createnewlanguage" tabindex="-1" role="dialog" aria-labelledby="createnewlanguage" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog" style="width:70%">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Select languages for this module </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="table-responsive">' +
                                    '<table id="language_table" class="table table-hover" style="text-align:center">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<td><b>Language</b></td>' +
                                                '<td><b>Select</b></td>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody>' +
                                        '</tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<span class="errorSelectLang errorClss pull-left">Please select at least one language</span>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div class="newlanguage_submit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="modal fade" id="add_section" tabindex="-1" role="dialog" aria-labelledby="add_section" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Add Section </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-6">' +
                                        '<div class="form-group">' +
                                            '<label for="sect_id" class="control-label">Section Code</label><span class="clsRed">*</span>' +
                                            '<input id="sect_id" type="text" name="section-id" class="form-control">' +
                                            '<span class="errorClss errorSecId">Required</span>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="col-xs-6">' +
                                        '<div class="form-group">' +
                                            '<label for="section_name" class="control-label">Section Name</label><span class="clsRed">*</span>' +
                                            '<input id="section_name" type="text" name="section-name" class="form-control">' +
                                            '<span class="errorClss errorSecName">Required</span>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<span class="errorSecCre errorClss pull-left">Error on creating. Try Again..!</span>' +
                            '<div class="btn btn-default" align="center" data-dismiss="modal"> Cancel </div>' +
                            '<div class="appsectionsubmit btn btn-default"> Submit </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'  +
            '<div class="modal fade" id="addSlotsModal" tabindex="-1" role="dialog" aria-labelledby="addSlotsModal" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content" style="border:10px solid #003264">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Slots Management </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="table-responsive" style="max-height:300px; overflow:scroll;">' +
                                            '<table id="addtobatchlist" class="table table-hover display">' +
                                                '<thead>' +
                                                    '<tr>' +
                                                        '<th>Slot Name</th>' +
                                                        '<th></th>' +
                                                '</thead>' +
                                                '<tbody>' +
                                                '</tbody>' +
                                                '<tfoot>' +
                                                    '</tr>' +
                                                    '<tr>' +
                                                        '<th colspan="2" class="errorOnDeleteSlot errorClss"> Problem in Deleting slot. Try Later..!</th>' +
                                                    '</tr>' +
                                                '</tfoot>' +
                                            '</table>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div class="form-group col-xs-11">' +
                                            '<label for="slotname" class="control-label">Slot Name</label><span class="clsRed">*</span> <span class="pull-right">Example: Slot (9 to 10)</span>' +
                                            '<input id="slotname" type="text" name="slot-name" class="form-control">' +
                                            '<span class="errorslotname errorClss">Please type the slot name</span>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<span class="errorSlotCre errorClss pull-left">Error on creating. Try Later..!</span>' +
                            '<div class="btn btn-default slotCancel"> Cancel </div>' +
                            '<div id="slotAddModule" class="btn btn-default disabal"> Submit </div>' +
                        '</div>'+
                    '</div>'+
                '</div>'
            );
            moduleconfigindex.moduleConfigurationDetails();
        },
        
        reloadExistingPage: function() {
            window.location.reload();
        },
    
    /*Add New Topic to Module*/
        newTopicForModule: function() {
            $(".errorClss").hide();
            $("#createnewtopic").modal({ keyboard: false });
            $("#createnewtopic").modal('show');
            $("#topicname").val("");
            $(".edittopic_submit").removeClass('edittopic_submit').addClass('newtopic_submit');
            $("#topics_table").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listtopic?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        for( var i = 0; i < data.length; i++){
                            $("#topics_table").children("tbody").append(
                                '<tr value='+data[i].uniId+'>' +
                                    '<td>'+ data[i].shortDesc+'</td>' +
                                    '<td><button class="btn btn-default editTopic"> Edit </button></td>' +
                                '</tr>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        /*Add New QuestionBank to Module*/
        newQuestionBankForModule: function() {
            $(".errorClss").hide();
            $("#createnewquestionbank").modal({ keyboard: false });
            $("#createnewquestionbank").modal('show');
            $("#questionbankset").val("");
            $(".editquestionbank_submit").removeClass('editquestionbank_submit').addClass('newquestionbank_submit');
            $("#questionbank_table").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listqbsbymodid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        for( var i = 0; i < data.length; i++){
                            $("#questionbank_table").children("tbody").append(
                                '<tr value='+data[i].qbsId+'>' +
                                    '<td>'+ data[i].shortDesc+'</td>' +
                                    '<td><button class="btn btn-default editquestionbank"> Edit </button></td>' +
                                '</tr>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        
        
        
        /*sybmitting QuestionBank created*/
        submitNewQuestionBankForModule: function (e) {
            if($("#questionbankset").val() == 0){
                $(".errorquestionbankset").show();
                $(".errorinsubmit").hide();
            } else {
                var topicname 
                var json;
                $(".errorinsubmit").hide();
                $(".errorquestionbankset").hide();
                json={"moduleId":mid,"shortDesc":$("#questionbankset").val()}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addqbstomodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        var triggerOnceTopic = true;
                        if(data == 1){
                            $("#createnewquestionbank").modal('hide');
                            moduleconfigindex.DisplayQuestionBankSelect();
                        } else {
                            $(".errorinsubmit").show();
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
    /*sybmitting Topic created*/
        submitNewTopicForModule: function (e) {
            if($("#topicname").val() == 0){
                $(".errortopicname").show();
                $(".errorinsubmit").hide();
            } else {
                var topicname 
                var json;
                $(".errorinsubmit").hide();
                $(".errortopicname").hide();
                json={"moduleId":mid,"shortDesc":$("#topicname").val()}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addtopictomodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        var triggerOnceTopic = true;
                        if(data == 1){
                            $("#createnewtopic").modal('hide');
                            moduleconfigindex.DisplayTopicToSelect();
                        } else {
                            $(".errorinsubmit").show();
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        
        
    /* Edit Topic for module submit */
        submitEditTopicForModule: function() {
            if($("#topicname").val() == 0){
                $(".errortopicname").show();
                $(".errorinsubmit").hide();
            } else {
                var topicname 
                var json;
                $(".errorinsubmit").hide();
                $(".errortopicname").hide();
                json={"moduleId":mid,"shortDesc":$("#topicname").val(),"uniId":$("#topicname").attr('value')}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "edittopictomodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        var triggerOnceTopic = true;
                        if(data == 1){
                            $("#createnewtopic").modal('hide');
                            moduleconfigindex.DisplayTopicToSelect();
                        } else {
                            $(".errorinsubmit").show();
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        /* Edit Question Bank for module submit */
        submitEditQuestionBankForModule: function() {
            if($("#questionbankset").val() == 0){
                $(".errorquestionbankset").show();
                $(".errorinsubmit").hide();
            } else {
                var topicname 
                var json;
                $(".errorinsubmit").hide();
                $(".errorquestionbankset").hide();
                json={"moduleId":mid,"shortDesc":$("#questionbankset").val(),"qbsId":$("#questionbankset").attr('value')}
                $("#pageloading").show();
                Backbone.ajax({
                    url: "editqbstomodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        var triggerOnceTopic = true;
                        if(data == 1){
                            $("#createnewquestionbank").modal('hide');
                            moduleconfigindex.DisplayQuestionBankSelect();
                        } else {
                            $(".errorinsubmit").show();
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        
    /*Add New Laguage for Module */
        newLanguageForModule: function() {
            $(".errorClss").hide();
            $("#createnewlanguage").modal({ keyboard: false });
            $("#createnewlanguage").modal('show');
            $("#language_table").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url: "maslanglist",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){

                    } else {
                        for( var i=0; i< data.length; i++ ){
                            $("#language_table").children("tbody").append(
                                '<tr id="lapre'+data[i].langId+'"><td>'+data[i].shortDesc+'</td><td><input type=checkbox class="checklanguage"></td></tr>'
                            );
                        }
                        $("#language_table").dataTable({
                            "ordering": false
                        });
                        $("#pageloading").show();
                        Backbone.ajax({
                            dataType: "json",
                            url: "modlanglist?moduleId="+mid+"",
                            data: "",
                            type: "POST",
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader(header, token);
                            },
                            success: function (data, textStatus, jqXHR) {
                                
                                if(data == 0){
                                    
                                } else{
                                    for( var i=0; i< data.length; i++ ){
                                        $("#lapre"+data[i].langId+"").children().children(".checklanguage").attr('checked', true)
                                    }
                                }
                            },
                            error: function (res, ioArgs) {
                                
                                if (res.status === 440) {
                                    window.location.reload();
                                }
                            },
                            complete: function() {
                                $("#pageloading").hide();
                            }
                        });
                    }
                },
                error: function (res, ioArgs) {
                    
                    if(res.status == 440){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
    /*Submitting Languages Selected*/
        submitNewLangForModule: function (e) {
            var json = [];
            var radios = $(".checklanguage"); 
            var count = 0;
            for (var i = 0, length = radios.length; i < length; i++) {
                if (radios[i].checked) {
                    json.push({"langId":radios[i].parentElement.parentElement.id.slice(5)});   
                }
            }
            if(json.length >= 1){
                $(".errorClss").hide();
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addmodlang?moduleId="+mid,
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data, textStatus, jqXHR) {
                        
                        $("#createnewlanguage").modal('hide');
                        if(data == 1){
                            window.location.reload();
                        } else {
                            $(".errorOnAddLanguage").show();
                        }
                    },
                    error: function (res, ioArgs) {
                        
                        if (res.status === 440) {
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            } else {
                $(".errorSelectLang").show();
            }
        },
        
    /*Display Language in Module */
        
        /*** GENERATE THE QUESTION LIST ALREADY IN ***/
        DisplayLanguageInConfig: function() {
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "modlanglist?moduleId="+mid+"",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    languageElement = Backbone.$("#language_select").empty();
                    Backbone.$("#lang-inst").empty();
                    if(data == 0){
                        
                    } else {
                        moduleconfigindex.generateLanguageList(data);
                    }
                },
                error: function (res, ioArgs) {
                        
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    moduleconfigindex.DisplayTopicToSelect();
                }
            });
        },
    
    /*** GENERATE THE LANGUAGE LIST TO UNORDER LIST ***/
        generateLanguageList: function(data) {
            for (var i=0; i< data.length; i++){
                Backbone.$("#lang-inst").append('<option value='+data[i].langId+'>'+data[i].shortDesc+'</option>')
                if( i == data.length-1){
                    languageElement.append(
                        '<span>'+data[i].shortDesc+'</span>'
                    );
                } else {
                    languageElement.append(
                        '<span>'+data[i].shortDesc+'</span> , '
                    );
                }
            }
        },
        
    /*** GENERATE THE TOPIC LIST ALREADY IN ***/
        DisplayTopicToSelect: function() {
            
            topicElement = Backbone.$("#topic_select").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listtopic?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        moduleconfigindex.generateTopicList(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                	
                	 moduleconfigindex.DisplayQuestionBankSelect();	
                
                }
            });
        },
        
        
        /*** GENERATE THE Question List ALREADY IN ***/
        DisplayQuestionBankSelect: function() {
            
            topicElement = Backbone.$("#questionbank_select").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listqbsbymodid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        moduleconfigindex.generateQuestionBankList(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                 moduleconfigindex.DisplaySlotList();
                }
            });
        },
        
        /*** GENERATE THE Question Bank LIST ON ARRAY OR PANEL***/
        generateQuestionBankList: function(data) {
            var tpcid;
            for (var i=0; i< data.length; i++){
                
                if(i == data.length-1){
                    topicElement.append(
                        '<span>'+data[i].shortDesc+'</span>'
                    );
                } else {
                    topicElement.append(
                        '<span>'+data[i].shortDesc+'</span> , '
                    );
                }
            }
        },
        
    /*** GENERATE THE TOPIC LIST ON ARRAY OR PANEL***/
        generateTopicList: function(data) {
            var tpcid;
            for (var i=0; i< data.length; i++){
                
                if(i == data.length-1){
                    topicElement.append(
                        '<span>'+data[i].shortDesc+'</span>'
                    );
                } else {
                    topicElement.append(
                        '<span>'+data[i].shortDesc+'</span> , '
                    );
                }
            }
        },
        
    /*** GENERATE THE Slot LIST ALREADY IN ***/
        DisplaySlotList: function() {
            
            Backbone.$("#slots-available").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getslotsbymoduleid",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                        $("#slots-available").text("No Slots Available")
                    } else{
                        moduleconfigindex.generateSlotList(data);
                    }
                },
                error: function (res, ioArgs) {
                                        if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    moduleconfigindex.sectionListInModule();
                }
            });
        },
        
    /*** GENERATE THE TOPIC LIST ON ARRAY OR PANEL***/
        generateSlotList: function(data) {
            var tpcid;
            for (var i=0; i< data.length; i++){
                
                if(i == data.length-1){
                    $("#slots-available").append(
                        '<span>'+data[i].slotName+'</span>'
                    );
                } else {
                    $("#slots-available").append(
                        '<span>'+data[i].slotName+'</span> , '
                    );
                }
            }
        },
        
    /* Edit Topic Name */
        editTopicForModule: function(e) {
            var tpcId = $(e.currentTarget.parentElement.parentElement).attr('value');
            var tpcName = $($(e.currentTarget.parentElement.parentElement).children('td')[0]).text();
            
            $("#createnewtopic").modal({ keyboard: false });
            $("#createnewtopic").modal('show');
            $("#topicname").val(tpcName);
            $("#topicname").attr('value', tpcId);
            $(".newtopic_submit").removeClass('newtopic_submit').addClass('edittopic_submit');
        },
    
        
        
        /* Edit questionbank set  */
        editQuestionBankForModule: function(e) {
            var tpcId = $(e.currentTarget.parentElement.parentElement).attr('value');
            var tpcName = $($(e.currentTarget.parentElement.parentElement).children('td')[0]).text();
            
            $("#createnewquestionbank").modal({ keyboard: false });
            $("#createnewquestionbank").modal('show');
            $("#questionbankset").val(tpcName);
            $("#questionbankset").attr('value', tpcId);
            $(".newquestionbank_submit").removeClass('newquestionbank_submit').addClass('editquestionbank_submit');
        },
    
        
        
        
        
    /*Slot Modal Display*/
        addSlotModal: function() {
            $("#addSlotsModal").modal('show');
            $("#addSlotsModal").modal({ keyboard: false });
            $("#slotname").val("");
            moduleconfigindex.generateSlotOnModalWindow();
        },
        
     /* NO OF ATTEMENT TOGGLE ON CLICK ON TEST CHECK */
        noOfAttemptToggle: function() {
            if(!$('.testCheck').is(':checked')){
                $("#testAttempt").val("N/A");
                $("#testAttempt").prop('disabled', true);
                $(".nooftestattements").hide();
            } else if($('.testCheck').is(':checked')){
                $("#testAttempt").val(3);
                $("#testAttempt").prop('disabled', false);
                $(".nooftestattements").show();
            }
            
        },
        /*Enabling batch status */
        noOfBatchExtension: function() {
        	/*if(!$('.enableBatchExtensionCheck').is(':checked')){
               
                $("#batchExtension").prop('disabled', true);
                $("#batchExtensionhide").hide();
            } else if($('.enableBatchExtensionCheck').is(':checked')){
              
                $("#batchExtension").prop('disabled', false);
                $("#batchExtensionhide").show();
            }*/
        	
        	if($("input:radio[value='0'][class='batchExtensionCheck']").is(':checked')) {
                $("#batchExtensionhide").hide();
            } else if($("input:radio[value='1'][class='batchExtensionCheck']").is(':checked')) {
                $("#batchExtensionhide").show();
                $("#batchExtension").val();
            }
		},

    /*** TEST TIMING VALIDATION ***/
        testTimingValidation: function(e) {
            $(".errorClss").hide();
            if($("#"+e.currentTarget.id+"").val().length >= 1){
                if(!$.isNumeric($("#"+e.currentTarget.id+"").val())){
                    var myStr = $("#"+e.currentTarget.id+"").val();
                    myStr = myStr.replace(/\D+/g, '');
                    $(".integerError").show();
                    $("#"+e.currentTarget.id+"").val(myStr);
                }
            }
            if( e.currentTarget.id == "passPercent"){
                if($("#"+e.currentTarget.id+"").val() > 100){
                    $(".errorTestPassPercentGreter").show();
                    var str = $("#"+e.currentTarget.id+"").val();
                    var newStr = str.substring(0, str.length-1);
                    $("#"+e.currentTarget.id+"").val(newStr);
                }
            }
            
            if( e.currentTarget.id == "batchExtension"){
            	if(!$.isNumeric($("#"+e.currentTarget.id+"").val())){
                    var str = $("#"+e.currentTarget.id+"").val();
                    var newStr = str.substring(0, str.length-1);
                    $("#"+e.currentTarget.id+"").val(newStr);
            	}
                
            }
            if( e.currentTarget.id == "minimumattendance"){
            	if(!$.isNumeric($("#"+e.currentTarget.id+"").val())){
                var str = $("#"+e.currentTarget.id+"").val();
                var newStr = str.substring(0, str.length-1);
                $("#"+e.currentTarget.id+"").val(newStr);
              }
            	
            }
        },
    
    /*** MODULE CONFIGURATION JSON VERIFICATION ***/
        moduleConfigVerify: function(e) {
            $(".errorClss").hide();
            if($(".testCheck").is(':checked')) {
                if( $("#testtiming").val() == 0 ){
                    $(".errorTestDuration").show();
                } else if($("#testAttempt").val() == 0) {
                    $(".errorMaxAtm").show();
                } else if (!$(".multiCheck").is(':checked')){
                    $(".errorMultiCheck").show();
                } else if ($("#passPercent").val() == 0){
                    $(".errorTestPassPercent").show();
                } else if (!$(".practiceTest").is(':checked')){
                    $(".errorPracticeTest").show();
                } else {
                    moduleconfigindex.moduleConfigSubmit();
                }
            } else {
                moduleconfigindex.moduleConfigSubmit();
            }
        },
        
    /*** SUBMITING MODULE CONFIGURATION ***/
        moduleConfigSubmit: function() {
            $(".errorClss").hide();
            var ts = 0;
            var cs = 0;
            var tf = 0;
            var noa = 0;
            var pm = 0;
            var multiple;
            var prTest;
            var tcc = 0;
            var mna = 0;
            var pde = 0;
            var bed = 0;
            var bes =0;
            for (var i = 0, length = $(".multiCheck").length; i < length; i++) {
                if ($(".multiCheck")[i].checked) { 
                    multiple = $($(".multiCheck")[i]).val();
                }
            }
            for (var i = 0, length = $(".practiceTest").length; i < length; i++) {
                if ($(".practiceTest")[i].checked) { 
                    prTest = $($(".practiceTest")[i]).val();
                }
            }
            
            if($(".trainingCheck").is(':checked')){
                ts = 1;
            }
            
            if($(".compAnsCheck").is(':checked')){
                cs = 1;
            }
            
            if($(".tccStatusCheck").is(':checked')){
                tcc = 1;
            }
            
            if($(".prefDateOfExamCheck").is(':checked')){
                pde = 1;
            }
            
            /*if($(".enableBatchExtensionCheck").value() == 1){
                bes = 1;
            }*/
            
            if($("input:radio[value='0'][class='batchExtensionCheck']").is(':checked')) {
                bes = 0;
                bed = 0;
            } else if($("input:radio[value='1'][class='batchExtensionCheck']").is(':checked')) {
                bes = 1;
                bed = $("#batchExtension").val();
            }
            
            /*if(!$(".enableBatchExtensionCheck").is(':checked')){
                bed = 0;
               
            }else{
            	 bed = $("#batchExtension").val();
            }*/
            
           
            
            if($(".testCheck").is(':checked')){
                tf = 1;
                noa = parseInt($("#testAttempt").val());
                pm = parseInt($("#passPercent").val());
                bed=$("#batchExtension").val();
            } else {
                noa = 0;
                multiple = 0;
                $("#testtiming").val(0);
                pm = 0;
            }
            mna = $("#minimumattendance").val();
           
            var json;
            try{
                json={"testDuration":parseInt($("#testtiming").val()),"trainingStatus":ts,"testStatus":tf,"maxAttempt":noa,"moduleId":mid,"testPageView":multiple,"passMarks":pm,"practiceTest":prTest,"compareAnswer":cs,"tccStatusCheck":tcc,"pdeCheck":pde,"minAttendCount":mna,"batchExtensionStatus":bes,"batchExtensionDays":bed};
            }catch(e){}
            $("#pageloading").show();
            Backbone.ajax({
                url: "configurationformodule",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    
                    if(data == 1){
                        moduleconfigindex.moduleConfigurationDetails();
                        sweetAlert("Module configuration saved");
                    } else if(data == 2){
                        $(".errorOnConfigSave").show();
                    } else{
                        alert("Contact IT team");
                    }
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    
    /*** DISPLAY BACK OFFICE ADMIN  ***/
        moduleConfigurationDetails: function() {
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "moduleconfigdetails?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                        
                    } else{
                        moduleconfigindex.generateConfigTable(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    moduleconfigindex.DisplayLanguageInConfig();
                }
            });
        },
        
    /*~~~~ GENERATE TABLE ELEMENTS OF USERS FROM JSON moduleLIST~~~~*/
        generateConfigTable: function (data) {
            $("#testtiming").val(data.testDuration);
            $("#testAttempt").val(data.maxAttempt);
            $("#passPercent").val(data.passMarks);
            $("#minimumattendance").val(data.minAttendCount);
            $("#batchExtension").val(data.batchExtensionDays);
            
            if(data.tccStatusCheck == 1){
                $(".tccStatusCheck").prop('checked', true);
            } else if(data.tccStatusCheck == 0){ 
                $(".tccStatusCheck").prop('checked', false);
            }
            
            if(data.pdeCheck == 1){
                $(".prefDateOfExamCheck").prop('checked', true);
            } else if(data.pdeCheck == 0){ 
                $(".prefDateOfExamCheck").prop('checked', false);
            }
            
            if(data.testPageView == 1){
                $('input[name=multiple][value=1]').attr('checked', true);
            } else if(data.testPageView == 2) {
                $('input[name=multiple][value=2]').attr('checked', true);
            }
            
            if(data.practiceTest == 1){
                $('input[name=module-practicetest][value=1]').attr('checked', true);
            } else if(data.practiceTest == 2) {
                $('input[name=module-practicetest][value=2]').attr('checked', true);
            }
            
            if(data.testStatus == 1){
                $(".testCheck").prop('checked', true);
                $(".nooftestattements").show();
            } else if(data.testStatus == 0){ 
                $(".testCheck").prop('checked', false);
                $(".nooftestattements").hide();
            }
            
            if(data.trainingStatus == 1){
                $(".trainingCheck").prop('checked', true);
            } else if(data.testStatus == 0){ 
                $(".trainingCheck").prop('checked', false);
            }
            
            if(data.compareAnswer == 1){
                $(".compAnsCheck").prop('checked', true);
            } else if(data.testStatus == 0){ 
                $(".compAnsCheck").prop('checked', false);
            }
            
            if(data.batchExtensionStatus == 1){
            	$('input:radio[class=batchExtensionCheck][value="1"]').attr('checked',true);
                $("#batchExtensionhide").show();
            } else if(data.batchExtensionStatus == 0){ 
            	$('input:radio[class=batchExtensionCheck][value="0"]').attr('checked',true);
                $("#batchExtensionhide").hide();
            }
            
        },
        
        sectionListInModule: function(){
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listsectiontomodule",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                    } else{
                        Backbone.$("#section_list").empty();
                        for (var i=0; i< data.length; i++){
                            if(i == data.length-1){
                                Backbone.$("#section_list").append(
                                    '<span>'+data[i].sectionDesc+'</span>'
                                );
                            } else {
                                Backbone.$("#section_list").append(
                                    '<span>'+data[i].sectionDesc+'</span> , '
                                );
                            }
                        }
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        addANewsectionToApp: function() {
            $("#add_section").modal('show');
            $("#section_name").val("");
            $("#sect_id").val("");
        },
        
        submitNewSectionToApp: function() {
            $(".errorClss").hide();
            if ($("#sect_id").val() == 0){
                $(".errorSecId").show();
            } else if($("#section_name").val() == 0) {
                $(".errorSecName").show();
            } else {
                var json;
                var secid = $("#sect_id").val().toUpperCase();
                var secname = $("#section_name").val();
                json={"sectionCode":secid,"sectionDesc":secname};
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addsectiontomodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            window.location.reload();
                        } else if(data == 2){
                            $(".errorSecCre").show();
                        } 
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        addSlotToModule: function() {
            $(".errorClss").hide();
            if ($("#slotname").val() == 0){
                $(".errorslotname").show();
            } else {
                var json;
                json={slotName:$("#slotname").val()};
                $("#pageloading").show();
                Backbone.ajax({
                    url: "addslottomodule",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader(header, token);
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        
                        if(data == 1){
                            window.location.reload();
                        } else if(data == 2){
                            $(".errorSlotCre").show();
                        } else {
                            sweetAlert(data);
                        }
                    },
                    error:function(res,ioArgs){
                        if(res.status == 403){
                            window.location.reload();
                        }
                    },
                complete: function() {
                    $("#pageloading").hide();
                }
                });
            }
        },
        
        generateSlotOnModalWindow: function() {
            Backbone.$("#addtobatchlist").children("tbody").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "getslotsbymoduleid",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        
                        Backbone.$("#addtobatchlist").children("tbody").append("<tr><td colspan='2'>No Slots Available</td></tr>")
                    } else{
                        for( var i = 0; i < data.length; i++){
                            Backbone.$("#addtobatchlist").children("tbody").append(
                            '<tr>' +
                                '<td>'+data[i].slotName+'</td>' +
                                '<td id="sl-Id'+data[i].uniId+'"><button class="btn btn-default slot-delete"> Delete </button></td>' +
                            '</tr>'
                            );
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    
    /*DELETE SLOT FROM MODULE */
        deleteSlotsFromModule: function(e) {
            $(".errorClss").hide();
            var slotId = e.currentTarget.parentElement.id.slice(5);
            $("#pageloading").show();
            Backbone.ajax({
                url: "deleteslotfrommodule?uniId="+slotId,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 1){
                        moduleconfigindex.generateSlotOnModalWindow();
                    } else if (data == 2){
                        $(".errorOnDeleteSlot").show();
                    } else {
                        sweetAlert(data);
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        beforeImgUpload: function(e){
            $(".errorClss").hide();
            if($("#lang-inst").val() == 0){
                sweetAlert("Please select Language before upload")
            } else if($("#lang-inst").val() == null){
                sweetAlert("Please select Language before upload")
            } 
        },
        
        instrutionImgUpload:function(e){
            $("#pageloading").show();
            $(".errorClss").hide();
            var lnd = $("#lang-inst").val();
            var urlv = "instructionupload?"+csrf_url+"="+token+"&langId="+lnd;
            $(e.currentTarget).parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".instrutfile").parent();
                    $(".instrutfile").remove();
                    $(pf).append('<input name="filename" type="file" class="instrutfile" accept="image/jpeg" style="width: 90px; left: 15px;">')
                    if(data == 1){
                        sweetAlert("Image Uploaded Successfully");
                    } else if(data == 2){
                        $(".errorInstUploadFail").show();
                    } else{
                        alert("Contact IT team");
                    }
                },
                error:function(res,ioArgs){
                    $(".instrutfile").val("");
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
            
        },
        
        specialCharValidate: function(e){
            $(".errorSpecialChar").remove();
            if($("#sect_id").val().length >= 1){
                var myStr = $("#sect_id").val();
                if(!(myStr.match(/^[a-zA-Z0-9]*$/))){
                    myStr = myStr.replace(/[^a-zA-Z0-9]/g,'');
                    $("#sect_id").val(myStr);
                    $("#sect_id").parent().append('<span class="errorClss errorSpecialChar"> Special Charachter Not Allowed </span>');
                    $(".errorSpecialChar").show();
                }
            }
        },
    });
})();

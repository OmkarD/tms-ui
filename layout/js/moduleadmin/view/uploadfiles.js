(function () {
    
    var mid;
    var dateBOBatch = [];
    window.upload_details = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    upload_details.Views.fileupload = Backbone.View.extend({
        
        events: {
            'change .internalstudentfile': 'studentInternalListUpload',
            'change .externalstudentfile': 'studentExternalListUpload',
            'change .stdattedfile': 'studentAttendanceListUpload',
            'change .readingmaterialfile': 'pdfStudyMaterialsUpload',
            'click .bt_sel': 'btBoStudentUploading',
            'click #select_date': 'boDateValidation',
            'click .atteBatchCls': 'boInBatchAttedance',
            'mousedown .stdattedfile': 'boMouseDownSelectBatch',
            'click .deleteStudyMaterials': 'deleteStudyMaterialsMethod'
        },
        
        initialize: function () {
            _.bindAll(this, 'render');
        },
        
        render: function (modulid) {
            mid = modulid;
            this.$el.html(
            '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Upload Excel File <div class="btn goToLastPage"><i class="fa fa-reply-all"></i> Back</div></h1>'+					
            '</div>'+
        '</div>'+
        '<div class="col-md-6">'+
            '<div class="col-md-6">'+
                '<div class="panel panel-red">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-file-text-o fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Attendance list</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<button class="btn btn-default bt_sel"> Select Batch</button>' +
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-6">'+
                '<div class="panel panel-primary">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-th-list fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Internal students </div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-primary btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input name="filename" type="file" class="internalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width:90px">'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-6">'+
                '<div class="panel panel-blue">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-stack-overflow fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>External students</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-blue btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input name="filename" type="file" class="externalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width:90px">'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-6">'+
                '<div class="panel panel-yellow">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-stack-overflow fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Reading Materials</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-blue btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Upload  <input name="filename" type="file" class="readingmaterialfile" style="width:90px">'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="col-md-6">'+
            '<div class="panel">'+
                '<h4 class="page-header">Excel Template</h4>'+
                '<div class="panel-body">'+
                    '<div class="list-group-item">'+
                        '<a href="studentattendancelistuploadtemplate" target="_blank">Click Here</a> to dowload \"Student attendance\" master excel upload format'+
                    '</div>'+
                    '<div class="list-group-item">'+
                        '<a href="internalstudentlistuploadtemplate" target="_blank">Click Here</a> to download \"Internal student\" master excel upload format'+
                    '</div>'+
                    '<div class="list-group-item">'+
                        '<a href="externalstudentlistuploadtemplate" target="_blank">Click Here</a> to download \"External student\" master excel upload format'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="panel">'+
                '<h4 class="page-header">Study Materials</h4>'+
                '<div class="panel-body">'+
                    '<div id="study_materials"></div>' +
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="modal fade" id="attend_uploading" tabindex="-1" role="dialog" aria-labelledby="attend_uploading" aria-hidden="true" data-backdrop="static" style="margin-top:2%">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<h4 class="modal-title"> Select batch </h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="row">' +
                                '<div class="col-xs-6">' +
                                    '<input id="select_batch" type="text" name="select-batches" class="dropdown-toggle form-control" placeholder="Select Batch" data-toggle="dropdown" readonly >' +
                                    '<ul class="dropdown-menu drop-menu" id="display_batch" aria-labelledby="select_batch" style="text-align:justify;"></ul>' +
                                '</div>' +
                                /*'<div class="col-xs-4">' +
                                    '<div id="attendance_date">' +
                                        '<input id="select_date" type="text" name="select-date" class="form-control" placeholder="Select Date" readonly>' +
                                    '</div>' +
                                '</div>' +*/
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="col-xs-9">' +
                                '<div class="btn btn-default pull-right" align="center" data-dismiss="modal" style="width: 90px"> Cancel </div>' +
                            '</div>'+
                            '<form method="post" enctype="multipart/form-data">'+
                                '<div class="input-group-btn">'+
                                    '<div class="btn panel-red btn-file">'+
                                        '<i class="glyphicon glyphicon-folder-open">'+
                                        '</i> &nbsp;Browse  <input name="filename" type="file" class="stdattedfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width:90px">   '+
                                    '</div>'+
                                '</div>'+
                            '</form>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'
            );
            this.loadStudyMaterial();
        },

        btBoStudentUploading: function(e) {
            $("#attend_uploading").modal('show');
            $("#attend_uploading").modal({ keyboard: false });
            this.attendanceBatchCreate();
            $("#select_batch").val("");
            $("#select_date").val("");
        },
        
        attendanceBatchCreate: function () {
            
            Backbone.$("#display_batch").empty();
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "listbatchbymid?moduleId="+mid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        
                    } else {
                        for (var i=0; i< data.length; i++){
                            Backbone.$("#display_batch").append(
                                '<li id="bth'+data[i].batchId+'" class="atteBatchCls" tabindex='+(i+1)+'>'+data[i].shortDesc+'</li>'
                            );
 dateBOBatch.push({"batchId":data[i].batchId,"startDate":data[i].validFrom,"endDate":data[i].validTo});
                        }
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        boDateValidation: function(e) {
            $(".attError").remove();
            var dt = parseInt($("#select_date").val().replace(/\D+/g, ''));
            var bhid = $("#select_batch").attr('value');
            if( $("#select_batch").val() == 0 ){
                $(e.currentTarget.parentElement).append('<div class="errorClss attError">Select Batch First</div>');
                $(".attError").show();
                $("#select_date").val("");
            }
        },
        
        boInBatchAttedance: function(e) {
            var bhname = $("#"+e.currentTarget.id+"").text();
            var bhid = e.currentTarget.id.slice(3);
            for ( var i=0; i< dateBOBatch.length; i++ ){
                if( dateBOBatch[i].batchId == bhid){
                    $("#attendance_date").empty().append(
                        '<input id="select_date" type="text" name="select-date" class="form-control" placeholder="Select Date" readonly>'
                    );
                    var mida = dateBOBatch[i].startDate;
                    var mxda = dateBOBatch[i].endDate;
                    $("#select_date").datepicker({
                        dateFormat: "yy-mm-dd",
                        minDate: mida,
                        maxDate: mxda
                    });
                }
            }
            $("#select_batch").prop('value', bhname);
            $( "div" ).data( "batchId", bhid );
        },
        
        boMouseDownSelectBatch: function (e){
            if(/*($("#select_date").val() == 0) ||*/ ($("#select_batch").val() == 0)){
                $("#attend_uploading").modal('hide');
                swal({
                    title:"Please Select Batch and date before uploading",
                },
                     function(){
                    $("#attend_uploading").modal('show');
                    $("#attend_uploading").modal({ keyboard: false });
                });
            }
        },

    /*** STUDY MATERIAL UPLOAD ***/
        pdfStudyMaterialsUpload: function(e) {
            $("#pageloading").show(); 
            var urlv = "uploadstudymaterial?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".readingmaterialfile").parent();
                    $(".readingmaterialfile").remove();
                    $(pf).append('<input name="filename" type="file" class="readingmaterialfile" style="width: 90px; left: 15px;">')
                    
                    if(data=="1"){
                        swal({
                            title:"Reading Material upload success",
                        },
                             function(){
                            window.location.reload();
                        });
                    } else if(data=="2"){
                        sweetAlert("Reading Material upload failed");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".readingmaterialfile").val("");
                    
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
        },
        
    /*** STUDENTS ATTEDANCE LIST UPLOAD ***/
        studentAttendanceListUpload:function(e){
            $("#pageloading").show();
            var bhid = $( "div" ).data( "batchId");
            //var dtd = $("#select_date").val();
            var urlv = "studentattedancelistupload?"+csrf_url+"="+token+"&moduleId="+mid+"&batchId="+bhid;//+"&date="+dtd;
            $(e.currentTarget).parent().parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".stdattedfile").parent();
                    $(".stdattedfile").remove();
                    $(pf).append('<input name="filename" type="file" class="stdattedfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    
                    if(data=="1"){
                        $("#attend_uploading").modal('hide');
                        swal({ 
                            title:"Student attendance uploaded successful",
                        },
                             function(){
                            $("#attend_uploading").modal('hide');
                            window.location.reload();
                        });
                    } else if(data=="2"){
                        $("#attend_uploading").modal('hide');
                        swal({
                            title:"Student attendance upload failed",
                        },
                             function(){
                            $("#attend_uploading").modal('show');
                            $("#attend_uploading").modal({ keyboard: false });
                        });
                    } else{
                        $("#attend_uploading").modal('hide');
                        $("#attend_uploading").on('hidden.bs.modal', function(){
                            sweetAlert(data);
                        });
                    }
                },
                error:function(res,ioArgs){
                    $(".stdattedfile").val("");
                    
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
        },
    
    /*** STUDENTS INTERNAL EMPLOYE ID LIST UPLOAD ***/
        studentInternalListUpload:function(e){
            $("#pageloading").show();
            var urlv = "internalstudentlistupload?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".internalstudentfile").parent();
                    $(".internalstudentfile").remove();
                    $(pf).append('<input name="filename" type="file" class="internalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    
                    if(data=="1"){
                        swal({
                            title:"Internal student employee list uploaded successfully",
                        },
                             function(){
                            window.location.reload();
                        });
                    } else if(data=="2"){
                        sweetAlert("Internal student employee list upload failed");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".internalstudentfile").val("");
                    
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
        },
        
    /*** STUDENT EXTERNAL EMPLOYE ID LIST UPLOAD ***/
        studentExternalListUpload:function(e){
            $("#pageloading").show();
            var urlv = "externalstudentlistupload?"+csrf_url+"="+token+"&moduleId="+mid;
            $(e.currentTarget).parent().parent().parent().prop("action",urlv).ajaxForm({
                success : function(data) {
                    var pf = $(".externalstudentfile").parent();
                    $(".externalstudentfile").remove();
                    $(pf).append('<input name="filename" type="file" class="externalstudentfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv" style="width: 90px; left: 15px;">')
                    
                    if(data=="1"){
                        swal({
                            title:"External student employee list uploaded successfully",
                        },
                             function(){
                            window.location.reload();
                        });
                    } else if(data=="2"){
                        sweetAlert("External student employee list upload failed");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                    $(".externalstudentfile").val("");
                    
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                },
                dataType : "text"
            }).submit();
        },
    
    /*LOAD STUDY MATERIALS*/
        loadStudyMaterial: function(){
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "studymatrial",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0) {
                        
                    } else {
                        $("#study_materials").empty();
                        for( var i = 0; i< data.length; i++){
                            $("#study_materials").append('<a href="getstudymatrial?unid='+data[i].unid+'" target="_blank">'+data[i].filename+'</a> <button class="btn btn-default deleteStudyMaterials" value="'+data[i].unid+'"> Delete </button><br>');
                        }
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        deleteStudyMaterialsMethod: function(e) {
        	$("#pageloading").show();
        	var unid = e.currentTarget.value;
        	Backbone.ajax({
                dataType: "json",
                url: "deletestudymaterial?unid="+unid,
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    window.location.reload();
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    });
})();
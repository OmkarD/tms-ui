
(function (){
    
    var modulArray = [];
    
    window.select_role = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };
    
    select_role.Views.header = Backbone.View.extend({
        
        events:{
            'click #loginapp': 'loadLoginInAsAppAdmin',
            'click .moducls': 'loadLoginInAsAppAdmin',
            'click .traincls': 'loadLoginInAsAppAdmin',
            'click .bocls': 'loadLoginInAsAppAdmin'
        },
        
        initialize: function () {
            _.bindAll(this,'render');
        },
        
        render: function () {
            this.$el.empty().append(
            '<div class="row">' +
                    '<div class="col-xs-8 col-xs-offset-1"  style="margin-top: 25px;">' +
                        '<div class="panel">' +
                            '<h3 class="page-header">Welcome to training portal</h3><br>' +
                            '<div class="panel-body">' +
                                '<div class="row">' +
                                    '<div class="col-xs-12">' +
                                        '<div id="select_wrapper" class="list-group" style="font-size: 18px;">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>');
            $(".logo-tms").text("TRAINING MANAGEMENT SYSTEM");
        },
        
        startWelcome: function(e) {
            
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "employeedetails",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    
                    if(data == 0){
                        sweetAlert("please login again");
                    } else {
                        $("#sidebar").empty()
                        $("#appuser_name").text(data.name);
                        $("#appuser_name").attr('value', data.employeeId);
                        var mjson;
                        mjson={"key":"partyName","value":data.name};
                        selectindex.sendDataToBackEndForFutureUse( mjson );
                        localStorage.setItem("empId", data.employeeId);
                        
                    }
                },
                error: function (res, ioArgs) {
                    
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                    selectindex.moduleForParticipant();
                }
            });
        },
        
        onloginSuccess: function (mid){
            var mjson;
            mjson={"key":"reloadMid","value":mid};
            selectindex.sendDataToBackEndForFutureUse( mjson );
        },
        
        
        moduleForParticipant: function(){
            $("#pageloading").show();
            Backbone.ajax({
                dataType: "json",
                url: "empmoddule",
                data: "",
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0){
                        sweetAlert("You don't have access to this layer")
                    } else {
                        $("#select_wrapper").empty();
                        if(data.length == 1){
                            var ur = data[0].role;
                            var mObj = data[0].modules;
                            if(mObj.length == 1){
                                var mid = mObj[0].moduleId;
                                var mname = mObj[0].shortDesc;
                                selectindex.loadLoginInDirectlyAdmin(ur, mid, mname);
                            } else if(mObj == 0){
                                var mid = 0;
                                var mname = 0;
                                selectindex.loadLoginInDirectlyAdmin(ur, mid, mname);
                            } else {
                                selectindex.generateModulSelection(data);
                            }
                        }else if(data.length > 1){
                            selectindex.generateModulSelection(data);
                        }
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        generateModulSelection: function(data){
            for(var i=0; i<data.length; i++){
                if(i == 0){
                    $("#select_wrapper").empty().append(
                        '<div class="list-group-item" style="background-color: #FCFCFC"><b><u>Select Your Role</u></b></div>'
                    );
                }
                var mObj = data[i].modules;
                if(data[i].role == 1){
                    $("#select_wrapper").append(
                        '<div class="col-lg-12 list-group-item">' +
                            '<a id="loginapp" value="0">Click Here</a> to proceed as application admin' +
                        '</div>'
                    );
                } else if(data[i].role == 2){
                    var mObj = data[i].modules;
                    for(var j=0; j<mObj.length; j++){
                        modulArray.push({"modulId":mObj[j].moduleId,"mName":mObj[j].shortDesc});
                        $("#select_wrapper").append(
                            '<div class="col-lg-12 list-group-item">' +
                                '<a id='+mObj[j].moduleId+' class="moducls" value='+mObj[j].moduleId+'>Click Here</a> to proceed as '+ mObj[j].shortDesc+' module admin' +
                            '</div>'
                        );
                    }
                } else if(data[i].role == 3){
                    var mObj = data[i].modules;
                    for(var j=0; j<mObj.length; j++){
                        modulArray.push({"modulId":mObj[j].moduleId,"mName":mObj[j].shortDesc});
                        $("#select_wrapper").append(
                            '<div class="col-lg-12 list-group-item">' +
                                '<a id='+mObj[j].moduleId+' class="traincls" value='+mObj[j].moduleId+'>Click Here</a> to proceed as '+ mObj[j].shortDesc+' Trainer' +
                            '</div>'
                        );
                    }
                } else if(data[i].role == 4){
                    var mObj = data[i].modules;
                    for(var j=0; j<mObj.length; j++){
                        modulArray.push({"modulId":mObj[j].moduleId,"mName":mObj[j].shortDesc});
                        $("#select_wrapper").append(
                            '<div class="col-lg-12 list-group-item">' +
                                '<a id='+mObj[j].moduleId+' class="bocls" value='+mObj[j].moduleId+'>Click Here</a> to proceed as '+ mObj[j].shortDesc+' Back office user' +
                            '</div>'
                        );
                    }
                }
            }
        },
        
        loadLoginInDirectlyAdmin: function(role, modid, modname) {
            $("#module_name").attr('value', modid);
            var json;
            var mjson;
            var mName;
            if(modname == 0){
                mName = "TRAINING MANAGEMENT SYSTEM";
            } else {
                mName = "TRAINING MANAGEMENT SYSTEM";
            }
            mjson={"key":"moduleName","value":"TRAINING MANAGEMENT SYSTEM"};
            selectindex.sendDataToBackEndForFutureUse( mjson );
            $(".logo-tms").text(mName);
            json={"userRole":role,"moduleId":modid};
            $("#pageloading").show();
            Backbone.ajax({
                url: "loginedas",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    selectindex.onloginSuccess(modid);
                    if(data == 0) {
                        sweetAlert("Your don't have access to this layer")
                    } else if( data == 1) {
                        selectindex.applicationAdminConfirm(data);
                    } else if( data == 2) {
                        selectindex.moduleAdminConfirm(data);
                    } else if( data == 3) {
                        selectindex.trainerAdminConfirm(data);
                    } else if( data == 4) {
                        selectindex.boAdminConfirm(data);
                    }
                },
                error: function (res, ioArgs) {
                                        if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        loadLoginInAsAppAdmin: function(e) {
            var checkrole = e.currentTarget.className;
            var roleid;
            var mid = $("#"+e.currentTarget.id+"").attr('value');
            var mjson;
            var mName;
            for( var i=0; i< modulArray.length; i++){
                if(mid == modulArray[i].modulId){
                    mName = modulArray[i].mName;
                }
            }
            mjson={"key":"moduleName","value":"TRAINING MANAGEMENT SYSTEM"};
            selectindex.sendDataToBackEndForFutureUse( mjson );
            $(".logo-tms").text(mName);
            switch(checkrole){
                case "":
                    roleid = 1;
                    break;
                case "moducls":
                    roleid = 2;
                    break;
                case "traincls":
                    roleid = 3;
                    break;
                case "bocls":
                    roleid = 4;
                    break;
            }
            $("#module_name").attr('value', mid);
            var json;
            json={"userRole":roleid,"moduleId":mid};
            $("#pageloading").show();
            Backbone.ajax({
                url: "loginedas",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    selectindex.onloginSuccess(mid);
                    if(data == 0) {
                        sweetAlert("Your don't have access to this layer")
                    } else if( data == 1) {
                        selectindex.applicationAdminConfirm(data);
                    } else if( data == 2) {
                        selectindex.moduleAdminConfirm(data);
                    } else if( data == 3) {
                        selectindex.trainerAdminConfirm(data);
                    } else if( data == 4) {
                        selectindex.boAdminConfirm(data);
                    }
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
        
        applicationAdminConfirm: function(data) {
            landingroutes.navigate('roles/program_modal/0', {trigger: true});
        },
        
        moduleAdminConfirm: function() {
            var modid = $("#module_name").attr('value');
            landingroutes.navigate('roles/config_module/'+modid, {trigger: true});
        },
        
        trainerAdminConfirm: function() {
            var modid = $("#module_name").attr('value');
            landingroutes.navigate('roles/trainerbatchmanagement/'+modid, {trigger: true});
        },
        
        boAdminConfirm: function() {
            $("#sidebar").empty();
            var modid = $("#module_name").attr('value');
            landingroutes.navigate('roles/excel_upload/'+modid, {trigger: true});
        },
        
        sendDataToBackEndForFutureUse: function( backenddate ){
            $("#pageloading").show();
            Backbone.ajax({
                url: "addvariable",
                data: JSON.stringify(backenddate),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function (data, textStatus, jqXHR) {
                    if(data == 0) {
                        sweetAlert("Loging again. Some problem");
                    } 
                },
                error: function (res, ioArgs) {
                    if (res.status === 440) {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                }
            });
        },
    });
})();
var batchid;

    /*** Routes for the Program admin Page ***/
  var  sidenav = Backbone.Router.extend({
      routes: {
          '': 'index',
          'roles/:id/:modulid': 'roles',
          'trainerbatch/:id/:tab/:modulid/:stuid': 'trainerbatches',
          'batch/:id/:tab/:modulid/:stuid': 'batches',
          'hier/:id/:tab/:modulid/:stuid': 'hierarchies',
          'question/:id/:lid/:modulid': 'questions',
          'topic/:id/:modulid': 'topics',
          'session/:id': 'sessions',
          'attedance/:id/:btid': 'attedancesMark',
          'branchmap/:id': 'branchTrainersMap',
          '*other': 'default'
      },
      
      index: function() {
          
          $('.modal-backdrop.fade.in').hide();
          $("li").parent().children(".maindashboard").removeClass("maindashboard");
          $("#module_admin").addClass("maindashboard");
          selectindex = new select_role.Views.header();
          $("#page-wrapper").empty().append(selectindex.el);
          selectindex.render();
          selectindex.startWelcome();
          var mjson={"key":"moduleName","value":"TRAINING MANAGEMENT SYSTEM"};
          var jjson={"key":"usermanagement","value":"TRAINING MANAGEMENT SYSTEM"};
          this.setModuleName( mjson );
//          this.setNameModule( jjson );
          
      },
      
      trainerbatches: function(id, tab, modulid, stuid) {
          this.loadModuleName();
          this.ModuleloadName(modulid);
          
          $('.modal-backdrop.fade.in').hide();
          batchid = id;
          trainerbatchdetails = new trainer_batch_manage.Views.batchslist();
          $("#page-wrapper").empty().append(trainerbatchdetails.el);
          trainerbatchdetails.render(batchid, modulid);
          $("#batchmanagement").addClass("maindashboard");
          if(tab == "stdnt"){
              $(".studentclass").addClass("active");
              trainerbatchdetails.loadStudentTable();
          } else if(tab == "trines"){
              $(".trainerclass").addClass("active");
              trainerbatchdetails.loadTrainerTable();
          } else if(tab == "batchstudentattendence"){
              $(".studentclass").addClass("active");
              trainerbatchdetails.batchStudentAttenTable(stuid);
          }
          $("#page-wrapper").show('slide');
      },
      
      batches: function(id, tab, modulid, stuid) {
          this.loadModuleName();
          this.ModuleloadName(modulid);
          
          $('.modal-backdrop.fade.in').hide();
          batchid = id;
          batchdetails = new batch_manage.Views.batchslist();
          $("#page-wrapper").empty().append(batchdetails.el);
          batchdetails.render(batchid, modulid);
          $("#batchmanagement").addClass("maindashboard");
          if(tab == "stdnt"){
              $(".studentclass").addClass("active");
              batchdetails.loadStudentTable();
          } else if(tab == "trines"){
              $(".trainerclass").addClass("active");
              batchdetails.loadTrainerTable();
          } else if(tab == "batchstudentattendence"){
              $(".studentclass").addClass("active");
              batchdetails.batchStudentAttenTable(stuid);
          }else if(tab == "latmtobtm"){
              heirarchyviewatmindex = new Heirarchy_view.Views.atmlist();
              $("#page-wrapper").empty().append(heirarchyviewatmindex.el);
              heirarchyviewatmindex.render();
            } else if(tab == "btmbatchview"){
              heirarchyviewbtmindex =new Heirarchy_view.Views.btmlist(); 
              $("#page-wrapper").empty().append(heirarchyviewbtmindex.el);
              heirarchyviewbtmindex.render();
            }
          $("#page-wrapper").show('slide');
      },

      hierarchies :function(id, tab, modulid, stuid){
    	  if(tab == "batchlist"){
              heirarchyviewbatchlist =new Heirarchy_view.Views.batchlist(); 
              $("#page-wrapper").empty().append(heirarchyviewbatchlist.el);
              heirarchyviewbatchlist.render(id);
            }
      } ,
      
      
      topics: function(id, modulid){
          this.loadModuleName();
           this.ModuleloadName(modulid);
          this.loadSivenavTab(modulid);
          $("#ques_master").addClass("maindashboard");
          questionmasterindex = new question_master.Views.header();
          $("#page-wrapper").empty().append(questionmasterindex.el);
          questionmasterindex.selectionType(id, modulid);
      },
      
      questions: function(id, lid, modulid){
          this.loadModuleName();
           this.ModuleloadName(modulid);
          
          $("#ques_master").addClass("maindashboard");
          questionmasterindex = new question_master.Views.header();
          $("#page-wrapper").empty().append(questionmasterindex.el);
          questionmasterindex.questionDisplay(id, lid, modulid);
      },
      
      
      branchTrainersMap: function(id) {
          usermanageindex = new user_manage.Views.header();
          $("#page-wrapper").empty().append(usermanageindex.el);
          usermanageindex.trainerBranchMap(id);
      },
      
      
      
      roles: function (id, modulid) {
          this.loadModuleName();
           this.ModuleloadName(modulid);
          $("#page-wrapper").hide('slide');
          
          $('.modal-backdrop.fade.in').hide();
          $("li").parent().children(".maindashboard").removeClass("maindashboard");
          $("#"+id+"").addClass("maindashboard");
          if (id === "dashboard") {
              dashboardindex = new dash_board.Views.header();
              $("#page-wrapper").empty().append(dashboardindex.el);
              dashboardindex.render(modulid);
              
          } else if (id === "batchmanagement") {
              batchindex = new batch_manage.Views.header();
              $("#page-wrapper").empty().append(batchindex.el);
              batchindex.render(modulid);
              
          } else if (id === "trainerbatchmanagement") {
              trainerbatchindex = new trainer_batch_manage.Views.header();
              $("#page-wrapper").empty().append(trainerbatchindex.el);
              trainerbatchindex.render(modulid);
          
          } else if (id === "training_done") {
              trainerbatchindex = new trainer_batch_manage.Views.header();
              $("#page-wrapper").empty().append(trainerbatchindex.el);
              trainerbatchindex.trainerDetails(modulid);
              
          } else if (id === "admin_attedance") {
              adminattendancemarkindex = new admin_attendmark.Views.header();
              $("#page-wrapper").empty().append(adminattendancemarkindex.el);
              adminattendancemarkindex.render(modulid);
                
          } else if (id === "attend_manage") {
              attedenceindex = new attend_mark.Views.header();
              $("#page-wrapper").empty().append(attedenceindex.el);
              attedenceindex.render(modulid);
                
          } else if (id === "user_manage") {
              usermanageindex = new user_manage.Views.header();
              $("#page-wrapper").empty().append(usermanageindex.el);
              usermanageindex.render(modulid);
                
          } else if (id === "program_modal") {
              programmoduleindex = new program_module.Views.header();
              $("#page-wrapper").empty().append(programmoduleindex.el);
              programmoduleindex.render(modulid);
                
          } else if (id === "language_add") {
              programmoduleindex = new program_module.Views.header();
              $("#page-wrapper").empty().append(programmoduleindex.el);
              programmoduleindex.languagesDisplay(modulid);
                
          } else if (id === "slot_add") {
              programmoduleindex = new program_module.Views.header();
              $("#page-wrapper").empty().append(programmoduleindex.el);
              programmoduleindex.slotDisplay(modulid);
                
          } else if (id === "config_module") {
              moduleconfigindex = new config_module.Views.header();
              $("#page-wrapper").empty().append(moduleconfigindex.el);
              moduleconfigindex.render(modulid);
                
          } /*else if (id === "section_create") {
              moduleconfigindex = new config_module.Views.header();
              $("#page-wrapper").empty().append(moduleconfigindex.el);
              moduleconfigindex.sectionsDisplay(modulid);
                
          }*/ else if (id === "paper_create") {
              testpaperindex = new test_paper.Views.header();
              $("#page-wrapper").empty().append(testpaperindex.el);
              testpaperindex.render(modulid);
                
          }else if (id === "manage_user") {
        	  manageuserindex = new add_students.Views.header();
              $("#page-wrapper").empty().append(manageuserindex.el);
              manageuserindex.render(modulid);
                
          }
          else if (id === "ques_master") {
              questionmasterindex = new question_master.Views.header();
              $("#page-wrapper").empty().append(questionmasterindex.el);
              questionmasterindex.render(modulid);
                
          } else if (id === "excel_upload") {
              uploadview = new upload_details.Views.fileupload();
              $("#page-wrapper").empty().append(uploadview.el);
              uploadview.render(modulid);
          }else if (id === "hierarchyview") {
             heirarchyindex = new Heirarchy_view.Views.header();
              $("#page-wrapper").empty().append(heirarchyindex.el);
              heirarchyindex.render(modulid);
                
          }
          
       
          $("#page-wrapper").show('slide');
      },
        
      default: function (other) {
          alert("Doesn't know this url " + other + "");
      },
      
      setModuleName: function ( backenddate ) {
          $("#pageloading").show();
          Backbone.ajax({
            url: "addvariable",
            data: JSON.stringify(backenddate),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data, textStatus, jqXHR) {
                
                if(data == 0) {
                    sweetAlert("Loging from first problem in storing data");
                }
            },
            error: function (res, ioArgs) {
                 
                if (res.status === 440) {
                    window.location.reload();
                }
            },
            complete: function() {
                $("#pageloading").hide();
            }
        });
      },
      
    /*  setNameModule: function ( backenddate,modulid ) {
          $("#pageloading").show();
          Backbone.ajax({
            url: "getaddstudentcheck?empId="+localStorage.getItem("empId")+"&modulid="+modulid,
            data: JSON.stringify(backenddate),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data, textStatus, jqXHR) {
                
                if(data == 0) {
                    sweetAlert("Loging from first problem in storing data");
                }
            },
            error: function (res, ioArgs) {
                
                if (res.status === 440) {
                    window.location.reload();
                }
            },
            complete: function() {
                $("#pageloading").hide();
            }
        });
      },*/
      
      loadModuleName: function (e) {
          $("#pageloading").show();
          Backbone.ajax({
              dataType: "json",
              url: "getvariable?key=moduleName",
              data: "",
              type: "POST",
              beforeSend: function(xhr) {
                  xhr.setRequestHeader(header, token);
              },
              success: function(data){
                  $(".logo-tms").text(data);
              },
              complete: function() {
                  $("#pageloading").hide();
                  progadminindex.loadPartyName();
              }
          });
    },
    
    /*loadSivenavTab: function (modulid) {
        $("#pageloading").show();
        Backbone.ajax({
            dataType: "json",
            url: "getvariable?modulid="+modulid,
            data: "",
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function(data){
               if(data==0)
            	   {
            	   $("#manage_user").hide();
            	   }
               
               else
            	   {
            	   $("#manage_user").show();
            	   }
            },
            complete: function() {
                $("#pageloading").hide();
                progadminindex.loadPartyName();
            }
        });
  },*/
  
  ModuleloadName: function (modulid) {
      $("#pageloading").show();
      Backbone.ajax({
          dataType: "json",
          url: "getaddstudentcheck?empId="+localStorage.getItem("empId")+"&modulid="+modulid,
          data: "",
          type: "POST",
          beforeSend: function(xhr) {
              xhr.setRequestHeader(header, token);
          },
          success: function(data){
            if(data==0){
              $("#manage_user").hide();
            }
            else if(data==1){
              $("#manage_user").show();  
            }
            //  $(".logo-tms").text(data);
             
          },
          complete: function() {
              $("#pageloading").hide();
              progadminindex.loadPartyName();
          }
      });
},
  
});
  
    

    
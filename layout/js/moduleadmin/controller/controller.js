
var programadmin = Backbone.View.extend({
    
    events: {
        'click #menu-toggle': 'togglemenuEvent',
        'mouseover .sidebar-nav li': 'tooltipPlace',
        'click .maind': 'navigatSideBar',
        'click .goToLastPage': 'callLastLoadedPage',
        'click .selecid': 'selecTionNavigator'
    },
        
    tagName: 'div',
    
    id: 'wrapper',
        
    initialize: function () {
        _.bindAll(this, 'render');
        this.render();
    },
        
    render: function () {
        this.$el.html(
            '<!-- Navigation -->' +
            '<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">' +
                '<div class="img-responsive logo-exide"></div>' +
                '<div class="img-responsive logo-tms">TRAINING MANAGEMENT SYSTEM</div>' +
                //'<div class="img-responsive logo-vidya"></div>' +
                '<div class="navbar-header">' +
                    '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">' +
                        '<span class="sr-only">Toggle navigation</span>' +
                        '<span class="icon-bar"></span>' +
                        '<span class="icon-bar"></span>' +
                        '<span class="icon-bar"></span>' +
                    '</button>' +
                '</div>' +
                '<!-- /.navbar-header -->' +
                '<ul class="nav navbar-top-links navbar-right">' +
                    '<li>' +
                        '<span id="module_name"></span>'+
                    '</li>' +
                    '<li class="dropdown">' +
                        '<a class="dropdown-toggle" data-toggle="dropdown" href="#">' +
                            '<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>' +
                        '</a>' +
                    '<ul class="dropdown-menu dropdown-user">' +
                            '<li><i class="fa fa-user fa-fw" style="padding: 3px 20px; min-height: 0;"></i><span id="appuser_name" ></span>' +
                            '</li>' +
                            '<li class="divider"></li>' +
                            '<li ><a href="admin">' +
                                '<i class="fa fa-link fa-fw"></i>' +
                                    ' Switch Role</a>' +
                            '</li>' +
                            '<li class="divider"></li>' +
                            '<li ><a href="logout">' +
                                '<i class="fa fa-sign-out fa-fw"></i>' +
                                    'Logout</a>' +
                            '</li>' +
                        '</ul>' +
                        '<!-- /.dropdown-user -->' +
                    '</li>' +
                '</ul>' +
                '<div id="sidebar">' +
                '</div>' +
            '</nav>' +
            '<!-- Page Content -->' +
            '<div id="page-wrapper"></div>' +
            '<!-- /#wrapper -->'
        );
    },
    
    loadPartyName: function(e) {
        $("#pageloading").show();
        Backbone.ajax({
            dataType: "json",
            url: "getvariable?key=moduleName",
            data: "",
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function(data){
                $("#appuser_name").text(data);
            },
            complete: function() {
                $("#pageloading").hide();
            }
        });
    },
    
    togglemenuEvent: function (e) {
        e.preventDefault();
        if(asTogal==null){
        	asTogal=1;
        }else if(asTogal==1){
        	asTogal=2;
        }else if(asTogal==2){
        	asTogal=1;
        }
        
        Backbone.$(".sidebar-nav").toggleClass("toggled");
        Backbone.$("#menu-toggle").toggleClass("toggled");
        Backbone.$("#page-wrapper").toggleClass("toggled");
        Backbone.$(".content-hide").toggleClass("toggled");
        Backbone.$(".sidebar-nav li").toggleClass("toggled");
        Backbone.$(".navbar-default.sidebar").toggleClass("toggled");
    },
        
    tooltipPlace: function () {
        $(".sidebar-nav li").tooltip({placement : 'right'});
    },
    
    callLastLoadedPage: function(){
        window.history.back();
    },
    
    navigatSideBar: function (e) {
        $("#page-wrapper").removeClass("toggled");
        var x = e.currentTarget.id;
        $("#pageloading").show();
        Backbone.ajax({
            dataType: "json",
            url: "getvariable?key=reloadMid",
            data: "",
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function(data){
                $("#module_name").attr('value', data);
                landingroutes.navigate('roles/' + x +'/'+data, {trigger: true});
            },
            complete: function() {
                $("#pageloading").hide();
            }
        });
    }
});